from django.contrib import admin
from django.urls import path, include
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView

from core.settings import DJANGO_ENV


def trigger_error(request):
    division_by_zero = 1 / 0

urlpatterns = [
    path('sentry-debug/', trigger_error),
    path("django-admin/", admin.site.urls),
    path("api/", include("apps.users.urls")),
    path("api/chats/", include("apps.chats.urls")),
    path("api/messages/", include("apps.tg_messages.urls")),
    path("api/experts/", include("apps.experts.urls")),
    path("api/faq/", include("apps.faq.urls")),
    path("api/commands/", include("apps.commands.urls")),
    path("api/participants/", include("apps.participants.urls")),
    path("api/forbidden-words/", include("apps.forbidden_words.urls")),
    path("api/schema/", SpectacularAPIView.as_view(), name="schema"),
    path("api/schema/swagger-ui/", SpectacularSwaggerView.as_view(url_name="schema"), name="swagger-ui"),
    path("raise/", lambda _: 1 / 0),
]
if DJANGO_ENV == "development":
    urlpatterns.append(path("silk/", include("silk.urls", namespace="silk")))

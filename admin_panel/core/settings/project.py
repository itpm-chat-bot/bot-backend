from telebot import TeleBot

from core.settings.django import config

bot: TeleBot = TeleBot(token=config("BOT_TOKEN"), parse_mode="HTML")

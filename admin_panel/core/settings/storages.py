from storages.backends.s3boto3 import S3Boto3Storage

from core.settings.django import config

AWS_S3_ENDPOINT_URL = config("S3_ENDPOINT_URL")
AWS_S3_ACCESS_KEY_ID = config("S3_ACCESS_KEY_ID")
AWS_S3_SECRET_ACCESS_KEY = config("S3_SECRET_ACCESS_KEY")
AWS_S3_URL_PROTOCOL = config("AWS_S3_URL_PROTOCOL", default="https:")
AWS_S3_CUSTOM_DOMAIN = config("AWS_S3_CUSTOM_DOMAIN", default=None)
AWS_QUERYSTRING_AUTH = False


class MediaStorage(S3Boto3Storage):
    bucket_name = config("PUBLIC_BUCKET_NAME")
    location = "media"
    default_acl = "public-read"


class StaticStorage(S3Boto3Storage):
    bucket_name = config("PUBLIC_BUCKET_NAME")
    location = "static"
    default_acl = "public-read"


class PrivateStorage(S3Boto3Storage):
    bucket_name = config("PRIVATE_BUCKET_NAME")
    location = "user_files"


STORAGES = {
    "default": {
        "BACKEND": "core.settings.storages.MediaStorage",
    },
    "staticfiles": {
        "BACKEND": "core.settings.storages.StaticStorage",
    },
}

from datetime import timedelta

REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": ("rest_framework_simplejwt.authentication.JWTStatelessUserAuthentication",),
    "DEFAULT_SCHEMA_CLASS": "drf_spectacular.openapi.AutoSchema",
}

SPECTACULAR_SETTINGS = {
    "TITLE": "ITPM admin panel API",
    "DESCRIPTION": "Admin panel for Telegram chat bot, which will help to improve user interactions with the chats.",
    "VERSION": "1.0.0",
    "SCHEMA_PATH_PREFIX": r"/api/",
}

# JWT settings
SIMPLE_JWT = {
    "ACCESS_TOKEN_LIFETIME": timedelta(minutes=15),
    "REFRESH_TOKEN_LIFETIME": timedelta(days=7),
    "LEEWAY": 30,
}

import sentry_sdk

from django.core.exceptions import DisallowedHost
from sentry_sdk.integrations.celery import CeleryIntegration
from sentry_sdk.integrations.django import DjangoIntegration

from core.settings.django import config, DJANGO_ENV

if DJANGO_ENV in ("production", "stage"):
    sentry_sdk.init(
        dsn=config("SENTRY_SDK"),
        integrations=[DjangoIntegration(), CeleryIntegration()],
        # If you wish to associate users to errors (assuming you are using
        # django.contrib.auth) you may enable sending PII data.
        send_default_pii=True,
        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        traces_sample_rate=1.0,
        # Set profiles_sample_rate to 1.0 to profile 100%
        # of sampled transactions.
        # We recommend adjusting this value in production.
        profiles_sample_rate=1.0,
        ignore_errors=[DisallowedHost],
    )

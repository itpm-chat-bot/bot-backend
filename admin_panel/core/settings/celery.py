from celery.schedules import crontab

from core.settings.django import config

USERS_COUNT_NOTIFICATION_CHAT_ID = config("USERS_COUNT_NOTIFICATION_CHAT_ID", cast=int, default=0)
USERS_COUNT_NOTIFICATION_CHAT_TOPIC = config("USERS_COUNT_NOTIFICATION_CHAT_TOPIC", cast=int, default=0)
USERS_COUNT_NOTIFICATION_HOUR = config("USERS_COUNT_NOTIFICATION_HOUR", cast=str, default="11")

CELERY_BROKER_URL = config("CELERY_BROKER_URL")
CELERY_RESULT_BACKEND = CELERY_BROKER_URL
CELERY_CACHE_BACKEND = "default"
CELERY_BROKER_TRANSPORT_OPTIONS = {"visibility_timeout": 30 * 86400}

CELERY_BEAT_SCHEDULE = {
    "update-users-every-hour": {
        "task": "apps.users.tasks.update_all_users",
        "schedule": crontab(hour="*", minute="30"),
    },
    "update-chats-every-hour": {
        "task": "apps.chats.tasks.update_all_chats",
        "schedule": crontab(hour="*", minute="35"),
    },
    "send-users-count-notification": {
        "task": "apps.users.tasks.send_users_count_notification",
        "schedule": crontab(hour=USERS_COUNT_NOTIFICATION_HOUR, minute="0"),
    },
    "refresh-night-time-every-day": {
        "task": "apps.chats.tasks.restart_night_time_in_chats",
        "schedule": crontab(hour="0", minute="0"),
    },
    # "update-chat-users-statistics": {
    #     "task": "apps.participants.management.commands.update_participants_statistics",
    #     "schedule": crontab(hour="0", minute="0"),
    # },
}

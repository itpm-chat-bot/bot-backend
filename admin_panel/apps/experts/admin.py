from django.contrib import admin

from .models import Expert


class ExpertAdmin(admin.ModelAdmin):
    list_display = ("username", "full_name", "chat", "updated")
    list_filter = ("chat",)
    search_fields = ("full_name", "username")
    ordering = ("-updated",)


admin.site.register(Expert, ExpertAdmin)

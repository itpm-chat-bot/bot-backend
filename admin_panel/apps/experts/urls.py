from rest_framework import routers

from .views import ExpertViewSet

app_name = "experts"

router = routers.SimpleRouter()
router.register("", ExpertViewSet)

urlpatterns = router.urls

from django.core.validators import RegexValidator
from django.db import models

from apps.chats.models import Chat
from apps.experts.validators import validate_unsupported_html


class Expert(models.Model):
    """Stores expert info"""

    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, db_index=True)
    username = models.CharField(max_length=32, validators=[RegexValidator(r"^@[a-zA-Z0-9_]+$")])
    full_name = models.CharField(max_length=32, blank=True, null=True, validators=[validate_unsupported_html])
    description = models.CharField(max_length=144, blank=True, null=True, validators=[validate_unsupported_html])
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.username} (chat: {self.chat.title})"

    class Meta:
        constraints = [models.UniqueConstraint(fields=["chat", "username"], name="unique_expert_per_chat")]

from rest_framework.exceptions import ValidationError


def filter_queryset_by_chat(queryset, chat_id=None, chat_tg_id=None):
    """Filter queryset by chat id or chat tg id."""
    if not chat_id and not chat_tg_id:
        raise ValidationError(detail="chat or chat_tg_id parameter is required")
    if chat_id:
        queryset = queryset.filter(chat=chat_id)
    else:
        queryset = queryset.filter(chat__tg_id=chat_tg_id)
    return queryset

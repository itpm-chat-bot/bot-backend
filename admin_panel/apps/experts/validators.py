from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from apps.tg_messages.utils import HTMLUtils


def validate_unsupported_html(value):
    """
    Validate that the value does not contain unsupported HTML tags.

    Args:
        value: The value.

    Raises:
        ValidationError: If the value contains HTML.

    """
    if HTMLUtils.remove_non_telegram_html(value) != value:
        raise ValidationError(_("Unsupported HTML tags are not allowed."))

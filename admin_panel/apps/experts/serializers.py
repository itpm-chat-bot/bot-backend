from rest_framework.serializers import ModelSerializer

from .models import Expert


class ExpertSerializer(ModelSerializer):
    class Meta:
        model = Expert
        exclude = ("created", "updated")

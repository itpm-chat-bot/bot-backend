# Generated by Django 4.2.4 on 2023-12-05 18:17

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("experts", "0003_alter_expert_full_name"),
    ]

    operations = [
        migrations.AddField(
            model_name="expert",
            name="call_period",
            field=models.CharField(blank=True, max_length=32, null=True),
        ),
        migrations.AddField(
            model_name="expert",
            name="delete_after_seconds",
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="expert",
            name="number_calls",
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]

from drf_spectacular.utils import extend_schema_view, extend_schema, OpenApiParameter
from rest_framework.mixins import DestroyModelMixin, CreateModelMixin, ListModelMixin, UpdateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet

from .models import Expert
from .serializers import ExpertSerializer
from .utils import filter_queryset_by_chat
from ..tg_messages.permissions import IsMessageChatAdmin
from ..users.permissions import IsTelegramBot


@extend_schema_view(
    list=extend_schema(
        description="List of experts created by user (filtered by chat).",
        parameters=[
            OpenApiParameter(name="chat", type=int, description="Chat id", required=False),
            OpenApiParameter(name="chat_tg_id", type=int, description="Chat telegram id", required=False),
        ],
    )
)
class ExpertViewSet(GenericViewSet, ListModelMixin, CreateModelMixin, DestroyModelMixin, UpdateModelMixin):
    """Experts viewset."""

    queryset = Expert.objects.all().order_by("-id")
    serializer_class = ExpertSerializer

    def get_queryset(self):
        """Queryset of experts created by user (filtered by chat)."""
        queryset = self.queryset
        if self.request.user.is_authenticated:
            queryset = queryset.filter(chat__admins__user_id=self.request.user.id)
        if self.action == "list":
            chat_id = self.request.query_params.get("chat")
            chat_tg_id = self.request.query_params.get("chat_tg_id")
            queryset = filter_queryset_by_chat(queryset, chat_id=chat_id, chat_tg_id=chat_tg_id)
        return queryset

    def get_permissions(self):
        if self.action == "list":
            permission_classes = (IsTelegramBot | IsAuthenticated,)
        else:
            permission_classes = (IsTelegramBot | IsAuthenticated & IsMessageChatAdmin,)
        return [permission() for permission in permission_classes]

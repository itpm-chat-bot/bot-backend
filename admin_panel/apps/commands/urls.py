from rest_framework import routers

from .views import CommandViewSet, AttachedCommandFileViewSet

app_name = "commands"

router = routers.SimpleRouter()
router.register("files", AttachedCommandFileViewSet)
router.register("", CommandViewSet)

urlpatterns = router.urls

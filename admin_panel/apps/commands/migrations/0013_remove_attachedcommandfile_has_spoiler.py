# Generated by Django 4.2.4 on 2024-05-27 05:42

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("commands", "0012_rename_attachedfile_attachedcommandfile"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="attachedcommandfile",
            name="has_spoiler",
        ),
    ]

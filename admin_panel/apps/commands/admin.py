from django.contrib import admin

from .models import Command, AttachedCommandFile


class AttachedFileAdmin(admin.TabularInline):
    """
    Admin inline interface for the AttachedFile model.

    Displays and configures the admin inline interface for the AttachedFile model within
    the Command admin page in the Django admin site.
    """
    model = AttachedCommandFile


class CommandAdmin(admin.ModelAdmin):
    """
    Admin interface for the Command model.

    Displays and configures the admin interface for the Command model in the Django admin site.
    """
    model = Command
    inlines = [AttachedFileAdmin]
    list_display = ("command_name", "command_text", "chat", "updated")
    list_filter = ("chat",)
    search_fields = ("command_name", "command_text")
    ordering = ("-updated",)


admin.site.register(Command, CommandAdmin)

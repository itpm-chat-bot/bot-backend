from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator
from django.db import models
from django.db.models.signals import post_save, pre_delete
from django.utils.translation import gettext_lazy as _

from telebot.types import BotCommand, BotCommandScopeChat, BotCommandScopeChatAdministrators

from apps.chats.models import Chat
from apps.tg_messages.const import FilesTypes, ALLOWED_ATTACHMENT_CAPTION_SIZE, ALLOWED_FILES_COUNT
from apps.tg_messages.validators import (
    validate_buttons,
    validate_file_size,
    validate_text_size,
    validate_media,
)
from core.settings import bot, PrivateStorage
from utils.random_filename import RandomFileName
from utils.redis import redis


class Command(models.Model):
    """Stores command info."""

    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, db_index=True)
    command_name = models.CharField(max_length=32)
    command_description = models.CharField(max_length=256)
    command_text = models.TextField(validators=[validate_text_size], blank=True, null=True)
    number_calls = models.IntegerField(blank=True, null=True)
    call_period = models.CharField(max_length=32, blank=True, null=True)
    delete_after_seconds = models.IntegerField(blank=True, null=True)
    allowed_access = models.CharField(blank=True, null=True)
    call_counter = models.IntegerField(default=0, blank=True)
    buttons = models.JSONField(blank=True, null=True, validators=[validate_buttons])
    disable_command = models.BooleanField(default=False)
    
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.command_name} (chat: {self.chat.title})"


class AttachedCommandFile(models.Model):
    """Stores information about files attached to a command."""

    file = models.FileField(
        upload_to=RandomFileName("attached_files"),
        storage=PrivateStorage(),
        validators=[
            FileExtensionValidator(
                FilesTypes.PICTURES_EXTENSIONS + FilesTypes.VIDEOS_EXTENSIONS + FilesTypes.DOCUMENTS_EXTENSIONS,
                _("The extension is not supported"),
            ),
            validate_file_size,
        ],
    )
    filename = models.CharField(max_length=255)
    as_media = models.BooleanField(default=False)
    command = models.ForeignKey(Command, on_delete=models.CASCADE, related_name="attachedfile_set")
    
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def clean(self):
        try:
            command = Command.objects.prefetch_related("attachedfile_set").get(pk=self.command_id)
        except Command.DoesNotExist:
            return
        files = command.attachedfile_set.all()
        if len(command.command_text) > ALLOWED_ATTACHMENT_CAPTION_SIZE:
            raise ValidationError("Command message text is too long. You can't add files to this commands message.")
        if command.buttons:
            raise ValidationError("Command must not have buttons.")
        if len(files) >= ALLOWED_FILES_COUNT:
            raise ValidationError("You can not add more than 10 files to one message.")
        if self.as_media:
            validate_media(self.file)
        if bool(files[0].as_media) != self.as_media:
            raise ValidationError(_("All files should be sent either as documents or as media not both."))

    def __str__(self):
        return f"{self.file.name}"


def get_commands_chat_administrators(sender, instance, **kwargs) -> list:
    """Get the list of the bot's commands for chat administrators."""
    try:
        if kwargs["deletion_status"]:
            custom_commands_for_chat_administrators = [
                BotCommand(command=command.command_name, description=command.command_description)
                for command in sender.objects.filter(chat=instance.chat)
                if command.command_name != instance.command_name and not command.disable_command
            ]
            return custom_commands_for_chat_administrators
    except KeyError:
        custom_commands_for_chat_administrators = [
            BotCommand(command=command.command_name, description=command.command_description)
            for command in sender.objects.filter(chat=instance.chat)
            if not command.disable_command
        ]
        return custom_commands_for_chat_administrators


def get_commands_chat_participants(sender, instance, **kwargs) -> list:
    """Get the list of the bot's commands for chat participants."""
    try:
        if kwargs["deletion_status"]:
            custom_commands_for_chat_participant = [
                BotCommand(command=command.command_name, description=command.command_description)
                for command in sender.objects.filter(chat=instance.chat)
                if command.allowed_access != "admin" and command.command_name != instance.command_name and not command.disable_command
            ]
            return custom_commands_for_chat_participant
    except KeyError:
        custom_commands_for_chat_participant = [
            BotCommand(command=command.command_name, description=command.command_description)
            for command in sender.objects.filter(chat=instance.chat)
            if command.allowed_access != "admin" and not command.disable_command
        ]
        return custom_commands_for_chat_participant


def set_commands_in_chat(sender, instance, **kwargs):
    """Set up the new list of the bot's commands in chat that include the new command."""
    command_descriptions = {
        "en": {"experts": "Chat experts", "faq": "FAQ", "support": "Support"},
        "ru": {"experts": "Эксперты чата", "faq": "Вопросы и ответы", "support": "Поддержка"},
    }

    try:
        user_key = f"language:{instance.main_admin.id}"  # noqa: E231
        language = redis.hget(name=user_key, key="language")
    except AttributeError:
        language = "ru"
    default_commands = [
        BotCommand(command="experts", description=command_descriptions[language]["experts"]),
        BotCommand(command="faq", description=command_descriptions[language]["faq"]),
    ]
    approved_default_commands_names = [
        command.command_name
        for command in sender.objects.filter(chat=instance.chat)
        if not command.disable_command
    ]
    approved_default_commands = []
    for command in default_commands:
        if command.command in approved_default_commands_names:
            approved_default_commands.append(command)
    approved_default_commands.append(
        BotCommand(command="support", description=command_descriptions[language]["support"])
    )

    try:
        if instance.allowed_access == "admin":
            chat_commands_for_admins = get_commands_chat_administrators(sender, instance, **kwargs)
            scope_admins = BotCommandScopeChatAdministrators(chat_id=instance.chat.tg_id)
        else:
            chat_commands_for_participants = get_commands_chat_participants(sender, instance, **kwargs)
            scope_participants = BotCommandScopeChat(chat_id=instance.chat.tg_id)
            commands = approved_default_commands + chat_commands_for_participants
            bot.set_my_commands(commands=commands, scope=scope_participants)

            scope_admins = BotCommandScopeChatAdministrators(chat_id=instance.chat.tg_id)
            chat_commands_for_admins = get_commands_chat_administrators(sender, instance, **kwargs)

        commands = approved_default_commands + chat_commands_for_admins
        bot.set_my_commands(commands=commands, scope=scope_admins)
    except Exception:
        return


def delete_commands_in_chat(sender, instance, **kwargs):
    """
    Delete the list of the bot's commands in chat and set up the new list of the bot's commands
    that does not include the deleted command.
    """
    kwargs["deletion_status"] = True
    try:
        if instance.allowed_access == "admin":
            bot.delete_my_commands(scope=BotCommandScopeChatAdministrators(chat_id=instance.chat.tg_id))
            return set_commands_in_chat(sender, instance, **kwargs)
        else:
            bot.delete_my_commands(scope=BotCommandScopeChat(chat_id=instance.chat.tg_id))
            return set_commands_in_chat(sender, instance, **kwargs)
    except Exception:
        return


post_save.connect(sender=Command, receiver=set_commands_in_chat)
pre_delete.connect(sender=Command, receiver=delete_commands_in_chat)

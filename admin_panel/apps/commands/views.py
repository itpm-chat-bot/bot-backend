from drf_spectacular.utils import extend_schema_view, extend_schema, OpenApiParameter
from rest_framework.decorators import action
from rest_framework.mixins import DestroyModelMixin, CreateModelMixin, ListModelMixin, UpdateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from .models import Command, AttachedCommandFile
from .serializers import (
    AttachedCommandFileAddSerializer,
    CommandSerializer,
    CommandCreateSerializer,
    EditCallCounterCommandSerializer,
)
from ..experts.utils import filter_queryset_by_chat
from ..tg_messages.permissions import IsMessageChatAdmin
from ..users.permissions import IsTelegramBot


@extend_schema_view(
    list=extend_schema(
        description="List of commands created by user (filtered by chat and command name).",
        parameters=[
            OpenApiParameter(name="chat", type=int, description="Chat id", required=False),
            OpenApiParameter(name="chat_tg_id", type=int, description="Chat telegram id", required=False),
            OpenApiParameter(name="command", type=str, description="Default or custom command name", required=False),
        ],
    )
)
class CommandViewSet(GenericViewSet, ListModelMixin, CreateModelMixin, DestroyModelMixin, UpdateModelMixin):
    """Commands viewset."""

    queryset = Command.objects.all().order_by("-id")

    def get_queryset(self):
        """Queryset of commands created by user filtered by chat and command name."""
        queryset = self.queryset
        if self.request.user.is_authenticated:
            queryset = queryset.filter(chat__admins__user_id=self.request.user.id)
        if self.action == "list":
            queryset = queryset.select_related("chat").prefetch_related("attachedfile_set")
            chat_id = self.request.query_params.get("chat")
            chat_tg_id = self.request.query_params.get("chat_tg_id")
            queryset = filter_queryset_by_chat(queryset, chat_id=chat_id, chat_tg_id=chat_tg_id)

            command = self.request.query_params.get("command")
            if command:
                queryset = queryset.filter(command_name=command)
        
        return queryset
    
    def get_serializer_class(self):
        """Returns the appropriate serializer class based on the action being performed."""
        if self.action in ("list", "retrieve"):
            return CommandSerializer
        else:
            return CommandCreateSerializer

    def get_permissions(self):
        if self.action == "list":
            permission_classes = (IsTelegramBot | IsAuthenticated,)
        else:
            permission_classes = (IsTelegramBot | IsAuthenticated & IsMessageChatAdmin,)
        return [permission() for permission in permission_classes]
    
    @extend_schema(
        request=EditCallCounterCommandSerializer,
        responses={200: EditCallCounterCommandSerializer, 403: str, 404: str},
    )
    @action(["PATCH"], detail=True)
    def update_call_counter(self, request: Request, *args, **kwargs):
        """Provides the ability to edit data about call counter of the command instance."""
        instance = self.get_object()
        serializer = EditCallCounterCommandSerializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


class AttachedCommandFileViewSet(GenericViewSet, CreateModelMixin, DestroyModelMixin, UpdateModelMixin):
    """Viewset for performing create and delete operations on attached files."""
    queryset = AttachedCommandFile.objects.all()
    serializer_class = AttachedCommandFileAddSerializer
    permission_classes = [IsAuthenticated,]

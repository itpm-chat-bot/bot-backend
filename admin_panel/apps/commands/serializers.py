from rest_framework.fields import IntegerField
from rest_framework.serializers import ModelSerializer, SerializerMethodField, PrimaryKeyRelatedField

from .models import Command, AttachedCommandFile
from ..chats.models import Chat
from apps.tg_messages.validators import (
    validate_text_or_attachment_provided,
    validate_file_group_has_no_buttons,
    validate_files_count,
    validate_files_caption_length,
    validate_files,
)


class AttachedFileInCommandSerializer(ModelSerializer):
    """Serializer for the AttachedCommandFile sent together with a command."""

    id = IntegerField(read_only=True)

    class Meta:
        model = AttachedCommandFile
        fields = ("id", "file", "as_media")


class AttachedCommandFileAddSerializer(AttachedFileInCommandSerializer):
    """Serializer for the single AttachedCommandFile posting."""

    command_id = PrimaryKeyRelatedField(queryset=Command.objects.all(), source="command")

    class Meta(AttachedFileInCommandSerializer.Meta):
        fields = ("id", "file", "as_media", "command_id")

    def create(self, validated_data):
        """Overrides the default create method of the ModelSerializer to save the name of the file."""
        validated_data["filename"] = validated_data["file"].name
        return super().create(validated_data)

    def update(self, instance, validated_data):
        """Overrides the default update method of the ModelSerializer to save the name of the file."""
        validated_data["filename"] = validated_data["file"].name
        return super().update(instance, validated_data)


class AttachedCommandFileRetrieveSerializer(AttachedFileInCommandSerializer):
    """Serializer for the AttachedCommandFile model retrieve."""

    file_size = SerializerMethodField()

    class Meta(AttachedFileInCommandSerializer.Meta):
        fields = ("id", "file", "filename", "file_size", "as_media")

    @staticmethod
    def get_file_size(obj: AttachedCommandFile) -> int:
        """Returns the file size in kilobytes."""
        if obj.file.size:
            size = int(obj.file.size / 1000)
            return size if size else 1
        else:
            return 0


class CommandSerializer(ModelSerializer):
    """The serializer responsible for creating, editing and getting command data."""
    chat_id = PrimaryKeyRelatedField(write_only=True, queryset=Chat.objects.all(), source="chat")
    number_calls = IntegerField(allow_null=True)
    delete_after_seconds = IntegerField(allow_null=True)
    files = AttachedCommandFileRetrieveSerializer(many=True, source="attachedfile_set")

    class Meta:
        model = Command
        exclude = ("created", "updated")


class CommandCreateSerializer(ModelSerializer):
    """Serializer for creating a new Command instance."""

    id = IntegerField(read_only=True)
    files = AttachedFileInCommandSerializer(many=True, required=False)

    class Meta:
        model = Command
        exclude = ("created", "updated")

    def validate(self, attrs: dict) -> dict:
        """
        Validates the Command fields.

        Args:
            attrs (dict): The serializer attributes.

        Returns:
            dict: The validated attributes.

        Raises:
            ValidationError: If the validation fails.
        """
        created = self.context["request"].method == "POST"
        text = attrs.get("command_text")
        buttons = attrs.get("buttons")
        if created:
            files = attrs.get("files", ())
            validate_files(files)
            validate_files_caption_length(files, text)
            validate_files_count(files)
            validate_file_group_has_no_buttons(files, buttons)
            validate_text_or_attachment_provided(files, text)
        else:
            files = self.instance.attachedfile_set.all()
            validate_files_caption_length(files, text)
            validate_file_group_has_no_buttons(files, buttons)
            validate_text_or_attachment_provided(files, text)
        return attrs

    def create(self, validated_data: dict) -> Command:
        """
        Overrides the default create method of the ModelSerializer to handle the creation
        of a new Command instance with attached files.

        Args:
            validated_data (dict): The validated data.

        Returns:
            Command: The created Command instance.
        """
        files = validated_data.pop("files", ())
        instance = Command.objects.create(**validated_data)
        for attached_file in files:
            attached_file["filename"] = attached_file["file"].name
            attached_file["message"] = instance
            AttachedCommandFile.objects.create(**attached_file)

        return instance

    def update(self, instance, validated_data):
        """
        Overrides the default update method of the ModelSerializer to handle the update
        of a Command instance with attached files.

        Args:
            instance (Command): The Command instance to update
            validated_data (dict): The validated data

        Returns:
            Command: The updated Command instance
        """
        instance = super().update(instance, validated_data)
        return instance


class EditCallCounterCommandSerializer(ModelSerializer):
    """The serializer responsible for editing the command call counter."""

    class Meta:
        model = Command
        fields = ("call_counter",)

    def update(self, instance, validated_data):
        instance.call_counter += validated_data.get("call_counter", 0)
        instance.save()
        return instance

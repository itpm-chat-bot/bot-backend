from django.contrib import admin

from .models import Participant, ChatUsersStatistics


class ParticipantAdmin(admin.ModelAdmin):
    list_display = ("id", "username", "first_name", "last_name", "updated")
    list_filter = ("chat", "status")
    search_fields = ("id", "username", "first_name", "last_name", "chat")
    ordering = ("-updated",)


admin.site.register(Participant, ParticipantAdmin)


class ChatUsersStatisticsAdmin(admin.ModelAdmin):
    list_display = ("chat", "date", "total_number_of_users", "number_of_active_users_for_last24hours")
    list_filter = ("chat", "date")
    search_fields = ("chat", "date")
    ordering = ("chat", "date")


admin.site.register(ChatUsersStatistics, ChatUsersStatisticsAdmin)

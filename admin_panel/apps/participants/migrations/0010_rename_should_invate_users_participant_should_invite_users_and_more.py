# Generated by Django 5.0.4 on 2024-04-11 18:47

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("participants", "0009_alter_participant_first_activity"),
    ]

    operations = [
        migrations.RenameField(
            model_name="participant",
            old_name="should_invate_users",
            new_name="should_invite_users",
        ),
        migrations.RenameField(
            model_name="participant",
            old_name="titul",
            new_name="titular",
        ),
        migrations.AlterField(
            model_name="participant",
            name="first_name",
            field=models.CharField(default="", max_length=64),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="participant",
            name="username",
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]

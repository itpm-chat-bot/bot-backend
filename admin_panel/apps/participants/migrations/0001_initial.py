# Generated by Django 4.2.4 on 2024-02-06 11:28

from django.db import migrations, models
import django.db.models.deletion
import utils.random_filename


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("chats", "0015_chat_main_admin"),
    ]

    operations = [
        migrations.CreateModel(
            name="Participant",
            fields=[
                ("id", models.BigIntegerField(primary_key=True, serialize=False, verbose_name="Telegram ID")),
                ("first_name", models.CharField(blank=True, max_length=64, null=True)),
                ("last_name", models.CharField(blank=True, max_length=64, null=True)),
                ("username", models.CharField(max_length=50)),
                ("status", models.CharField(blank=True, max_length=20, null=True)),
                (
                    "avatar",
                    models.ImageField(
                        blank=True, null=True, upload_to=utils.random_filename.RandomFileName("user_avatars")
                    ),
                ),
                ("avatar_id", models.CharField(blank=True, max_length=100, null=True)),
                ("rating", models.IntegerField(blank=True, null=True)),
                ("titul", models.CharField(blank=True, max_length=100, null=True)),
                ("additiomal_info", models.CharField(blank=True, max_length=100, null=True)),
                ("number_messages", models.IntegerField(blank=True, null=True)),
                ("number_links", models.IntegerField(blank=True, null=True)),
                ("number_files", models.IntegerField(blank=True, null=True)),
                ("message_quality", models.IntegerField(blank=True, null=True)),
                ("first_activity", models.DateField(blank=True, null=True)),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("updated", models.DateTimeField(auto_now=True)),
                ("chat", models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to="chats.chat")),
            ],
        ),
    ]

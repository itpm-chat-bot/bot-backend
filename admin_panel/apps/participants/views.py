from datetime import timedelta

from django.db.models import Q
from django.utils import timezone
from drf_spectacular.utils import extend_schema_view, extend_schema, OpenApiParameter
from rest_framework.decorators import action
from rest_framework.mixins import DestroyModelMixin, CreateModelMixin, ListModelMixin, UpdateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from .models import Participant
from .serializers import (
    ParticipantSerializer,
    EditNumberMessagesSerializer,
    EditAdminPermissionsSerializer,
)
from .utils import filter_queryset_by_id
from ..tg_messages.permissions import IsMessageChatAdmin
from ..users.permissions import IsTelegramBot


@extend_schema_view(
    list=extend_schema(
        description="List of participants.",
        parameters=[
            OpenApiParameter(name="chat", type=int, description="Chat id", required=False),
            OpenApiParameter(name="chat_tg_id", type=int, description="Chat telegram id", required=False),
            OpenApiParameter(name="participant_id", type=int, description="Participant telegram id", required=False),
            OpenApiParameter(
                name="sort_by_rating", type=bool, description="Sort chat participants by rating", required=False
            ),
            OpenApiParameter(
                name="active_participants", type=bool, description="Show only chat members", required=False
            ),
            OpenApiParameter(
                name="active_period",
                type=str,
                description="Show active users for specified period",
                required=False,
                enum=["day", "week", "month"],
            ),
            OpenApiParameter(
                name="administration", type=bool, description="Show only chat administrator and owner", required=False
            ),
            OpenApiParameter(
                name="restricted_participants",
                type=bool,
                description="Show only restricted chat participants",
                required=False,
            ),
        ],
    )
)
class ParticipantViewSet(GenericViewSet, ListModelMixin, CreateModelMixin, DestroyModelMixin, UpdateModelMixin):
    """Participants viewset."""

    queryset = Participant.objects.all()
    serializer_class = ParticipantSerializer

    def get_queryset(self):
        """Queryset of participants filtered by chat, participant or participant status."""
        queryset = self.queryset
        if self.request.user.is_authenticated:
            queryset = queryset.filter(chat__admins__user_id=self.request.user.id)
        if self.action == "list":
            chat_id = self.request.query_params.get("chat")
            chat_tg_id = self.request.query_params.get("chat_tg_id")
            participant_id = self.request.query_params.get("participant_id")
            queryset = filter_queryset_by_id(
                queryset,
                chat_id=chat_id,
                chat_tg_id=chat_tg_id,
                participant_id=participant_id,
            )

            sort_by_rating = self.request.query_params.get("sort_by_rating")
            if sort_by_rating and sort_by_rating == "true":
                queryset = queryset.order_by("-rating")
            else:
                queryset = queryset.order_by("rating")

            active_period = self.request.query_params.get("active_period")
            if active_period:
                current_time = timezone.now()
                if active_period == "day":
                    last_accepted_time = current_time - timedelta(days=1)
                elif active_period == "week":
                    last_accepted_time = current_time - timedelta(weeks=1)
                elif active_period == "month":
                    last_accepted_time = current_time - timedelta(days=30)
                else:
                    last_accepted_time = None

                if last_accepted_time:
                    queryset = queryset.filter(last_activity__range=(last_accepted_time, current_time))

            active_participants = self.request.query_params.get("active_participants")
            if active_participants:
                queryset = queryset.exclude(
                    Q(status=Participant.Status.ADMINISTRATOR)
                    | Q(status=Participant.Status.CREATOR)
                    | Q(status=Participant.Status.RESTRICTED)
                )

            administration = self.request.query_params.get("administration")
            if administration:
                queryset = queryset.exclude(
                    Q(status=Participant.Status.MEMBER) | Q(status=Participant.Status.RESTRICTED)
                )

            restricted_participants = self.request.query_params.get("restricted_participants")
            if restricted_participants:
                queryset = queryset.exclude(
                    Q(status=Participant.Status.ADMINISTRATOR)
                    | Q(status=Participant.Status.CREATOR)
                    | Q(status=Participant.Status.MEMBER)
                )

        return queryset

    def get_permissions(self):
        if self.action in (
            "list",
            "create",
            "update",
            "partial_update",
            "update_number_messages",
            "update_admin_permissions",
        ):
            permission_classes = (IsTelegramBot | IsAuthenticated,)
        else:
            permission_classes = (IsTelegramBot | IsAuthenticated & IsMessageChatAdmin,)
        return [permission() for permission in permission_classes]

    def get_object(self):
        """
        Returns the object the view is displaying.

        Changes the lookup_field to tg_id for bot compatibility for some actions.
        """
        if self.action in ("partial_update", "update", "update_number_messages", "update_admin_permissions"):
            self.lookup_field = "id"
            self.lookup_url_kwarg = "pk"
        return super().get_object()

    @extend_schema(
        request=EditNumberMessagesSerializer,
        responses={200: EditNumberMessagesSerializer, 403: str, 404: str},
    )
    @action(["PATCH"], detail=True)
    def update_number_messages(self, request: Request, *args, **kwargs):
        """
        Provides the ability to edit data about number of messages, links and files of the participant
        instance.
        """
        instance = self.get_object()
        serializer = EditNumberMessagesSerializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    @extend_schema(
        request=EditAdminPermissionsSerializer,
        responses={200: EditAdminPermissionsSerializer, 403: str, 404: str},
    )
    @action(["PATCH"], detail=True)
    def update_admin_permissions(self, request: Request, *args, **kwargs):
        """
        Provides the ability to edit data about permissions for the chat admin of the participant
        instance.
        """
        instance = self.get_object()
        serializer = EditAdminPermissionsSerializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

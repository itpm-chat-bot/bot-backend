from rest_framework.exceptions import ValidationError


def filter_queryset_by_id(queryset, chat_id=None, chat_tg_id=None, participant_id=None):
    """Filter queryset by chat id, chat tg id or participant tg id."""
    if not chat_id and not chat_tg_id and not participant_id:
        raise ValidationError(detail="chat, chat_tg_id or participant_tg_id parameter is required")
    if chat_id:
        queryset = queryset.filter(chat=chat_id)
    elif chat_tg_id:
        queryset = queryset.filter(chat__tg_id=chat_tg_id)
    else:
        queryset = queryset.filter(tg_id=participant_id)
    return queryset

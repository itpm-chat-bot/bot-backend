from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.chats.models import Chat
from utils.random_filename import RandomFileName


class Participant(models.Model):
    """Stores participant info"""

    class Status(models.TextChoices):
        RESTRICTED = "restricted"
        ADMINISTRATOR = "administrator"
        CREATOR = "creator"
        MEMBER = "member"

    tg_id = models.BigIntegerField(_("Telegram ID"), unique=False)
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64, blank=True, null=True)
    username = models.CharField(max_length=50, blank=True, null=True)
    status = models.CharField(max_length=20, blank=True, null=True, choices=Status.choices)
    avatar = models.ImageField(upload_to=RandomFileName("participant_avatars"), blank=True, null=True)
    avatar_id = models.CharField(max_length=100, blank=True, null=True)
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, db_index=True)
    chat_tg_id = models.BigIntegerField(unique=False)
    rating = models.IntegerField(blank=True, null=True)
    titular = models.CharField(max_length=64, blank=True, null=True)
    additional_info = models.CharField(max_length=64, blank=True, null=True, default="")
    number_chars = models.IntegerField(blank=True, null=True)
    number_messages = models.IntegerField(blank=True, null=True)
    number_reactions = models.IntegerField(blank=True, null=True)
    number_links = models.IntegerField(blank=True, null=True)
    number_files = models.IntegerField(blank=True, null=True)
    message_quality = models.IntegerField(blank=True, null=True)
    first_activity = models.DateField(auto_now_add=True, blank=True, null=True)
    last_activity = models.DateField(auto_now=True, blank=True, null=True)
    should_manage_messages = models.BooleanField(default=False)
    should_manage_participants = models.BooleanField(default=False)
    should_invite_users = models.BooleanField(default=False)
    should_configure_chat_bot = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.username} (chat: {self.chat.title})"


class ChatUsersStatistics(models.Model):
    """Stores info for number of chat participants for next statistics"""

    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, db_index=True)
    date = models.DateField(auto_now_add=True)
    total_number_of_users = models.IntegerField()
    number_of_active_users_for_last24hours = models.IntegerField()

    def __str__(self):
        return f"{self.chat.title} ({self.date})"

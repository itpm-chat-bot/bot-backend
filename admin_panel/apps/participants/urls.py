from rest_framework import routers

from apps.participants.views import ParticipantViewSet

app_name = "participants"

router = routers.SimpleRouter()
router.register("", ParticipantViewSet)

urlpatterns = router.urls

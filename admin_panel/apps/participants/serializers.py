from django.utils import timezone
from rest_framework.generics import get_object_or_404
from rest_framework.serializers import ModelSerializer

from apps.chats.models import Chat
from .models import Participant


class ParticipantSerializer(ModelSerializer):
    """The serializer responsible for creating, editing and getting chat participants data for bot."""

    class Meta:
        model = Participant
        exclude = ("created", "updated")

    def to_internal_value(self, data: dict):
        data = data.copy()
        chat_tg_id: list | int = data.get("chat")
        if isinstance(chat_tg_id, list):
            chat_tg_id = chat_tg_id[0]
        chat_instance: Chat = get_object_or_404(Chat, tg_id=chat_tg_id)
        data["chat"] = chat_instance.pk
        return super().to_internal_value(data)


class EditNumberMessagesSerializer(ModelSerializer):
    """
    The serializer responsible for editing number of messages, chars, links and files.
    Also updates last activity time.
    """

    class Meta:
        model = Participant
        fields = (
            "number_chars",
            "number_messages",
            "number_reactions",
            "number_links",
            "number_files",
        )

    def update(self, instance, validated_data):
        number_chars = validated_data.get("number_chars", 0)
        points = 0
        if 0 < number_chars <= 100:
            points = 1
        elif 101 <= number_chars <= 1000:
            points = 5
        elif 1001 <= number_chars <= 4096:
            points = 10
        points += (
            validated_data.get("number_links", 0) * 2
            + validated_data.get("number_files", 0) * 2
            + validated_data.get("number_reactions", 0)
        )

        instance.rating += points
        instance.number_chars += validated_data.get("number_chars", 0)
        instance.number_messages += validated_data.get("number_messages", 0)
        instance.number_reactions += validated_data.get("number_reactions", 0)
        instance.number_links += validated_data.get("number_links", 0)
        instance.number_files += validated_data.get("number_files", 0)
        instance.last_activity = timezone.now()
        instance.save()
        return instance


class EditAdminPermissionsSerializer(ModelSerializer):
    """The serializer responsible for editing permissions for the chat admin."""

    class Meta:
        model = Participant
        fields = (
            "should_manage_messages",
            "should_manage_participants",
            "should_invite_users",
            "should_configure_chat_bot",
        )

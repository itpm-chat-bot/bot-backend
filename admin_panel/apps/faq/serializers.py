from rest_framework.serializers import ModelSerializer, SerializerMethodField, BooleanField, PrimaryKeyRelatedField

from .models import FAQ
from ..chats.models import Chat
from ..tg_messages.utils import HTMLUtils


class FAQSerializer(ModelSerializer):
    answer_telegram = SerializerMethodField()
    is_draft = BooleanField(write_only=True, required=False)
    chat_id = PrimaryKeyRelatedField(write_only=True, queryset=Chat.objects.all(), source="chat")

    class Meta:
        model = FAQ
        fields = (
            "id",
            "chat_id",
            "question",
            "answer",
            "answer_telegram",
            "telegraph_link",
            "is_draft",
        )
        read_only_fields = ("id", "answer_telegram")

    @staticmethod
    def get_answer_telegram(obj) -> str:
        return HTMLUtils.remove_non_telegram_html(obj.answer)

# Generated by Django 4.2.4 on 2023-12-10 09:27

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("faq", "0004_alter_faq_answer_alter_faq_question"),
    ]

    operations = [
        migrations.AlterField(
            model_name="faq",
            name="answer",
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]

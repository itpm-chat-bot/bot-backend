from django.db import models

from apps.chats.models import Chat
from apps.experts.validators import validate_unsupported_html


class FAQ(models.Model):
    """FAQ model"""

    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, db_index=True)
    question = models.CharField(max_length=100, blank=True, null=True, validators=[validate_unsupported_html])
    answer = models.CharField(max_length=200, blank=True, null=True)
    telegraph_link = models.URLField(blank=True, null=True)
    is_draft = models.BooleanField(default=False, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.question}"

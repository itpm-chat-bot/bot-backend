from django.contrib import admin

from .models import FAQ


class FAQAdmin(admin.ModelAdmin):
    list_display = ("question", "chat", "updated")
    list_filter = ("chat",)
    search_fields = ("question", "answer")
    ordering = ("-updated",)


admin.site.register(FAQ, FAQAdmin)

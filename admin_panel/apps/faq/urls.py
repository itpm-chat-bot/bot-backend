from rest_framework import routers

from .views import FAQViewSet

app_name = "faq"

router = routers.SimpleRouter()
router.register("", FAQViewSet)

urlpatterns = router.urls

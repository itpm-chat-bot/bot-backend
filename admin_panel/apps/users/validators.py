import langcodes

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from pytz import timezone
from pytz.exceptions import UnknownTimeZoneError


def validate_ietf_lang_code(value: str):
    if not langcodes.tag_is_valid(value):
        raise ValidationError(
            _("%(value)s is not a correct IETF language code"),
            params={"value": value},
        )


def validate_timezone(value: str):
    try:
        timezone(value)
    except UnknownTimeZoneError:
        raise ValidationError(
            _("%(value)s is not a correct time zone"),
            params={"value": value},
        )

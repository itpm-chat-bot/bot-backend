import logging
from time import sleep
from celery import shared_task

from django.conf import settings
from django.db import models
from django.utils import timezone
from telebot.apihelper import ApiException

from core.settings import bot
from .models import UserProfile
from ..chats.models import Chat
from ..chats.utils import update_chat
from ..tg_messages.utils import TelegramBotUtils


@shared_task
def update_user_chats(user_id: int):
    """Celery task that updates chats info for all the chats where the user is admin."""
    user_chats = Chat.objects.filter(admins__id=user_id)
    for chat in user_chats:
        update_chat(chat)


@shared_task
def update_user_info(user_id: int):
    """Celery task that updates user info."""
    user = UserProfile.objects.get(id=user_id)
    try:
        user_tg_data = bot.get_chat(user_id)
    except ApiException as e:
        if "chat not found" in e.description:
            user.blocked_telegram_bot = True
            user.save()
            logging.info(f"{e} | User marked as blocked_telegram_bot. User id: {user.id}")
        else:
            logging.exception(e)
        return
    user.username = user_tg_data.username
    user.first_name = user_tg_data.first_name
    user.last_name = user_tg_data.last_name
    TelegramBotUtils.update_avatar(user, user_tg_data)
    user.blocked_telegram_bot = False
    user.save()


@shared_task
def update_all_users():
    """Celery task that updates all users info."""
    day_ago = timezone.now() - timezone.timedelta(days=1)
    users = UserProfile.objects.filter(
        blocked_telegram_bot=False,
    ) | UserProfile.objects.filter(blocked_telegram_bot=True, updated__lte=day_ago)
    for user in users:
        try:
            update_user_info(user.id)
        except Exception as e:
            logging.exception(e)
        sleep(0.1)


@shared_task
def send_users_count_notification():
    """Celery task that sends notification to the team about the number of users in the system."""
    if settings.USERS_COUNT_NOTIFICATION_CHAT_ID:
        users_count = UserProfile.objects.count()
        logged_in_users_count = UserProfile.objects.filter(has_first_login=True).count()
        groups_subscribers_count = Chat.objects.filter(is_bot_admin=True).aggregate(
            total_subscribers=models.Sum("participants_number")
        )["total_subscribers"]
        groups_count = Chat.objects.filter(is_bot_admin=True).count()

        bot.send_message(
            settings.USERS_COUNT_NOTIFICATION_CHAT_ID,
            f"<b>Users stats for {timezone.now().strftime('%d.%m.%Y')}:</b>\n\n"  # noqa: E231
            f"Started bot: {users_count}\n"
            f"Logged in admin panel: {logged_in_users_count}\n\n"
            f"Groups: {groups_count}\n"
            f"Total subscribers in all groups: {groups_subscribers_count}",
            message_thread_id=settings.USERS_COUNT_NOTIFICATION_CHAT_TOPIC or None,
        )

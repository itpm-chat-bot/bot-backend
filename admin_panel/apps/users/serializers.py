from rest_framework.fields import IntegerField, CharField
from rest_framework.serializers import ModelSerializer, Serializer

from .models import UserProfile, User


class ProfileSerializer(ModelSerializer):
    """Serializes profile data. It is used to get profile data by user."""

    class Meta:
        model = UserProfile
        fields = ("id", "first_name", "last_name", "username", "avatar", "language_code", "site_language", "timezone")


class ProfileEditSerializer(ModelSerializer):
    """Serializes profile data. It is used to edit profile data by bot."""

    id = IntegerField(read_only=True)

    class Meta:
        model = UserProfile
        exclude = ("user", "has_first_login", "created", "updated")


class ProfileSettingsSerializer(ModelSerializer):
    """Serializes profile data. It is used to edit profile settings by user."""

    class Meta:
        model = UserProfile
        fields = ("language_code", "site_language", "timezone")


class ProfileRegistrationSerializer(ModelSerializer):
    """Serializes profile data. It is used to create profile by bot."""

    class Meta:
        model = UserProfile
        exclude = ("has_first_login", "created", "updated")


class UserSerializer(ModelSerializer):
    """Serializes user data. It is used to create associated user model on registration."""

    class Meta:
        model = User
        fields = ("username",)


class TelegramLoginSerializer(Serializer):
    """Serializes telegram login data. It is used to log in user by telegram API."""

    id = IntegerField()
    first_name = CharField(allow_null=True, required=False)
    last_name = CharField(allow_null=True, required=False)
    username = CharField(allow_null=True, required=False)
    photo_url = CharField(allow_null=True, required=False)
    auth_date = IntegerField()
    hash = CharField()
    login_token = CharField(allow_null=True, required=False)

    def is_valid(self, *, raise_exception=False):
        """Checks if the data is valid."""
        if self.initial_data.get("login_token"):
            return True
        else:
            return super().is_valid(raise_exception=raise_exception)

from django.urls import path
from rest_framework.routers import SimpleRouter
from rest_framework_simplejwt.views import TokenRefreshView

from apps.users.views import login, UserProfileViewSet

app_name = "users"

router = SimpleRouter()
router.register("profiles", UserProfileViewSet)


urlpatterns = [
    path("login/", login, name="token_obtain_pair"),
    path("token-refresh/", TokenRefreshView.as_view(), name="token_refresh"),
] + router.urls

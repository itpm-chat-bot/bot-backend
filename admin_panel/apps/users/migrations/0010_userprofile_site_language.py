# Generated by Django 4.2.4 on 2023-11-07 18:02

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("users", "0009_alter_userprofile_first_name_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="userprofile",
            name="site_language",
            field=models.CharField(blank=True, default="ru", max_length=30, null=True),
        ),
    ]

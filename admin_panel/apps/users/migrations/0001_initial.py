# Generated by Django 4.1.7 on 2023-04-06 09:27

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name="UserProfile",
            fields=[
                ("id", models.BigIntegerField(primary_key=True, serialize=False, verbose_name="Telegram ID")),
                ("first_name", models.CharField(max_length=50)),
                ("last_name", models.CharField(blank=True, max_length=50)),
                ("username", models.CharField(blank=True, max_length=50)),
                ("language_code", models.CharField(max_length=10)),
                ("timezone", models.CharField(max_length=30)),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("updated", models.DateTimeField(auto_now=True)),
                (
                    "user",
                    models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
                ),
            ],
            options={
                "verbose_name": "User Profile",
                "verbose_name_plural": "User Profiles",
            },
        ),
    ]

from datetime import datetime

from django.conf import settings
from django.contrib import admin
from django.http import HttpResponseRedirect

from .models import UserProfile
from .utils import get_tokens_for_user, AdminToken


class ProfileAdmin(admin.ModelAdmin):
    list_display = ("id", "username", "first_name", "last_name", "has_first_login", "updated")
    list_filter = ("timezone", "language_code")
    search_fields = ("username", "first_name", "last_name", "id")
    ordering = ("-updated",)

    def response_change(self, request, obj):
        if "_get-jwt" in request.POST:
            token = get_tokens_for_user(obj.user)["access"]
            expires = (settings.SIMPLE_JWT["ACCESS_TOKEN_LIFETIME"] + datetime.now()).strftime("%H:%M:%S")
            self.message_user(request, f"Access token (expires at {expires}): {token}")
            return HttpResponseRedirect(".")
        elif "_login-as" in request.POST:
            if not request.user.has_perm("users.change_userprofile") or not request.user.has_perm("chats.change_chat"):
                self.message_user(request, "You don't have permission to login as another user", level="ERROR")
                return HttpResponseRedirect(".")
            token = AdminToken.generate_token(obj)
            return HttpResponseRedirect(f"/login?login_token={token}")
        return super().response_change(request, obj)


admin.site.register(UserProfile, ProfileAdmin)

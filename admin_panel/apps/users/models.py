from django.contrib.auth import get_user_model
from django.db import models
from django.db.models.signals import post_delete
from django.utils.translation import gettext_lazy as _

from utils.random_filename import RandomFileName
from .validators import validate_ietf_lang_code, validate_timezone

User = get_user_model()


class UserProfile(models.Model):
    """Stores profile info"""

    id = models.BigIntegerField(_("Telegram ID"), primary_key=True)
    user = models.OneToOneField(to=User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64, blank=True, null=True)
    username = models.CharField(max_length=50, blank=True, null=True)
    avatar = models.ImageField(upload_to=RandomFileName("user_avatars"), blank=True, null=True)
    avatar_id = models.CharField(max_length=100, blank=True, null=True)
    language_code = models.CharField(max_length=30, validators=[validate_ietf_lang_code], blank=True, null=True)
    site_language = models.CharField(max_length=30, blank=True, null=True, default="ru")
    timezone = models.CharField(max_length=30, blank=True, null=True, validators=[validate_timezone])
    has_first_login = models.BooleanField(default=False)
    blocked_telegram_bot = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.first_name} ({self.id})"

    class Meta:
        verbose_name = _("User Profile")
        verbose_name_plural = _("User Profiles")


def delete_associated_user_account(sender, instance, **kwargs):
    """Delete associated user account when deleting user profile"""
    try:
        instance.user.delete()
    except User.DoesNotExist:
        pass


post_delete.connect(sender=UserProfile, receiver=delete_associated_user_account)

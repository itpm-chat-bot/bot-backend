from rest_framework.permissions import BasePermission

from core.settings import config


class IsTelegramBot(BasePermission):
    """Allows access to bot."""

    def has_permission(self, request, view):
        return request.META.get("HTTP_AUTHORIZATION") == config("BOT_TOKEN")

from dateutil.relativedelta import relativedelta

from django.utils import timezone
from django.utils.crypto import salted_hmac
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from rest_framework_simplejwt.tokens import RefreshToken

from apps.users.models import User


def get_tokens_for_user(user: User) -> dict:
    """Returns JWT token for a user"""
    refresh = RefreshToken.for_user(user)
    return {
        "refresh": str(refresh),
        "access": str(refresh.access_token),
    }


class AdminToken:
    """Class for getting and validating admin token to login as another user"""

    ALGORITHM = "sha3_256"
    EXPIRATION_TIME = 60  # 1 minute

    @classmethod
    def generate_token(cls, user) -> str:
        """Generates token for a user"""
        expiration_time = (timezone.now() + relativedelta(seconds=cls.EXPIRATION_TIME)).timestamp()

        # Combine user ID and expiration time and encode it
        payload = urlsafe_base64_encode(f"{user.pk}-{expiration_time}".encode())

        sign = cls.generate_sign(payload)

        return f"{payload}-{sign}"

    @classmethod
    def generate_sign(cls, payload: str) -> str:
        """Generates a sign for a payload"""
        return salted_hmac(
            key_salt=cls.__name__,
            value=payload,
            algorithm=cls.ALGORITHM,
        ).hexdigest()

    @classmethod
    def get_user_id(cls, token: str) -> int | None:
        """Validates token and returns user ID derived from the token"""
        payload, sign = token.split("-")
        if sign != cls.generate_sign(payload):
            return None
        decoded_payload = urlsafe_base64_decode(payload).decode()
        user_id, expiration_time = decoded_payload.split("-")
        if float(expiration_time) < timezone.now().timestamp():
            return None
        return user_id

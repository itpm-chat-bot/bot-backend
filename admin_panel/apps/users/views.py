import logging

from django_telegram_login.authentication import verify_telegram_authentication
from django_telegram_login.errors import TelegramDataIsOutdatedError, NotTelegramDataError
from drf_spectacular.utils import extend_schema, OpenApiResponse
from rest_framework import mixins, status
from rest_framework.decorators import api_view, action
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from core.settings import config
from utils.redis import redis
from .models import UserProfile
from .permissions import IsTelegramBot
from .serializers import (
    UserSerializer,
    ProfileRegistrationSerializer,
    TelegramLoginSerializer,
    ProfileSerializer,
    ProfileSettingsSerializer,
    ProfileEditSerializer,
)
from .tasks import update_user_chats, update_user_info
from .utils import get_tokens_for_user, AdminToken

BOT_TOKEN = config("BOT_TOKEN")


class UserProfileViewSet(mixins.RetrieveModelMixin, GenericViewSet):
    """The viewset responsible for creating, editing and getting profile data."""

    queryset = UserProfile.objects.all()
    serializer_class = ProfileSerializer

    def get_permissions(self):
        """Instantiates and returns the list of permissions that this view requires."""
        if self.action == "me":
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsTelegramBot]
        return [permission() for permission in permission_classes]

    def partial_update(self, request: Request, *args, **kwargs) -> Response:
        """Allows to edit any profiles data."""
        partial = True
        instance = self.get_object()
        serializer = ProfileEditSerializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data)

    @staticmethod
    @extend_schema(
        responses={
            201: TokenObtainPairSerializer,
            400: str,
            409: OpenApiResponse(
                description="A user with such id already exists.",
                response=dict,
            ),
        }
    )
    def create(request: Request, *args, **kwargs) -> Response:
        """Creates user profile"""

        try:
            UserProfile.objects.get(id=request.data.get("id"))
        except UserProfile.DoesNotExist:
            pass
        else:
            return Response({"username": ["A user with such id already exists."]}, status.HTTP_409_CONFLICT)

        user_serializer = UserSerializer(data={"username": request.data.get("id")})
        user_serializer.is_valid(raise_exception=True)
        user = user_serializer.save()

        request_data = request.data.copy()
        request_data["user"] = user.pk

        profile_serializer = ProfileRegistrationSerializer(data=request_data)
        if not profile_serializer.is_valid():
            user.delete()
            return Response(profile_serializer.errors, status.HTTP_400_BAD_REQUEST)
        profile_serializer.save()
        return Response(profile_serializer.data, status=status.HTTP_201_CREATED)

    @action(detail=False, methods=["get", "patch"])
    def me(self, request: Request) -> Response:
        """Allows to get and edit settings of the currently authorized profile."""
        if request.method == "PATCH":
            instance = self.queryset.get(user=request.user.id)
            serializer = ProfileSettingsSerializer(instance, data=request.data, partial=True)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            user_key = f"language:{instance.id}"  # noqa: E231
            language = instance.site_language
            redis.hset(name=user_key, key="language", value=language)
        instance = self.queryset.get(user=request.user.id)
        serializer = self.serializer_class(instance=instance)
        return Response(serializer.data)


@extend_schema(
    parameters=[TelegramLoginSerializer],
    responses={200: TokenObtainPairSerializer, 400: dict, 403: str, 404: str},
)
@api_view(["GET"])
def login(request: Request) -> Response:
    """
    User authentication using telegram login widget.

    It also runs the tasks responsible for updating the user's chats and information on successful authentication.

    Returns JWT tokens.
    """
    serializer = TelegramLoginSerializer(data=request.query_params)
    if not serializer.is_valid():
        return Response(status=400, data=serializer.errors)

    if login_token := request.query_params.get("login_token"):
        profile_id = AdminToken.get_user_id(login_token)
        if not profile_id:
            return Response(status=403, data="Invalid login_token.")
        profile = UserProfile.objects.get(id=profile_id)

    else:
        try:
            result = verify_telegram_authentication(bot_token=BOT_TOKEN, request_data=request.query_params)
            profile = UserProfile.objects.get(id=result.get("id"))

        except TelegramDataIsOutdatedError as e:
            return Response(status=403, data=str(e))

        except NotTelegramDataError as e:
            return Response(status=403, data=str(e))

        except UserProfile.DoesNotExist:
            return Response(status=404, data="Account not found in database.")

        if not profile.has_first_login:
            profile.has_first_login = True
            profile.save()

    update_user_chats.delay(profile.id)
    try:
        update_user_info(profile.id)
    except Exception as e:
        logging.exception(e)

    tokens = get_tokens_for_user(profile.user)
    return Response(tokens)

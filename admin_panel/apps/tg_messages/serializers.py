from django.http import QueryDict
from rest_framework.relations import PrimaryKeyRelatedField
from rest_framework.serializers import IntegerField, ModelSerializer, SerializerMethodField

from .const import MESSAGE_TITLE_LENGTH
from .models import Message, AttachedFile
from .tasks import send_message, delete_message
from .utils import HTMLUtils
from .validators import (
    validate_text_or_attachment_provided,
    validate_file_group_has_no_buttons,
    validate_files_count,
    validate_files_caption_length,
    validate_files,
)


class AttachedFileInMessageSerializer(ModelSerializer):
    """Serializer for the AttachedFile sent together with a message."""

    id = IntegerField(read_only=True)

    class Meta:
        model = AttachedFile
        fields = ("id", "file", "has_spoiler", "as_media")


class AttachedFileAddSerializer(AttachedFileInMessageSerializer):
    """Serializer for the single AttachedFile posting."""

    message_id = PrimaryKeyRelatedField(queryset=Message.objects.all(), source="message")

    class Meta(AttachedFileInMessageSerializer.Meta):
        fields = ("id", "file", "has_spoiler", "as_media", "message_id")

    def create(self, validated_data):
        """Overrides the default create method of the ModelSerializer to save the name of the file."""
        validated_data["filename"] = validated_data["file"].name
        return super().create(validated_data)

    def update(self, instance, validated_data):
        """Overrides the default update method of the ModelSerializer to save the name of the file."""
        validated_data["filename"] = validated_data["file"].name
        return super().update(instance, validated_data)


class AttachedFileRetrieveSerializer(AttachedFileInMessageSerializer):
    """Serializer for the AttachedFile model retrieve."""

    file_size = SerializerMethodField()

    class Meta(AttachedFileInMessageSerializer.Meta):
        fields = ("id", "filename", "file_size", "as_media")

    @staticmethod
    def get_file_size(obj: AttachedFile) -> int:
        """Returns the file size in kilobytes."""
        if obj.file.size:
            size = int(obj.file.size / 1000)
            return size if size else 1
        else:
            return 0


class MessageRetrieveSerializer(ModelSerializer):
    """Serializer for the Message retrieve."""

    files = AttachedFileRetrieveSerializer(many=True, source="attachedfile_set")
    preview_text = SerializerMethodField()

    class Meta:
        model = Message
        exclude = ("created", "updated", "msg_id")

    @staticmethod
    def get_preview_text(obj: Message) -> str:
        """Returns html tags stripped message text."""
        return HTMLUtils.remove_html(obj.text)


class MessageThinSerializer(MessageRetrieveSerializer):
    """Serializer for a simplified representation of a Message model."""

    title = SerializerMethodField()

    class Meta:
        model = Message
        depth = 1
        fields = (
            "id",
            "title",
            "send_time",
            "actual_send_time",
        )

    @staticmethod
    def get_title(obj: Message) -> str:
        """Returns html tags stripped and truncated message text."""
        title = HTMLUtils.remove_html(obj.text)
        ellipsis_end = "..." if len(title) > MESSAGE_TITLE_LENGTH else ""
        return title[:MESSAGE_TITLE_LENGTH] + ellipsis_end


class MessageCreateSerializer(ModelSerializer):
    """Serializer for creating a new Message instance."""

    id = IntegerField(read_only=True)
    files = AttachedFileInMessageSerializer(many=True, required=False)

    class Meta:
        model = Message
        exclude = ("msg_id", "actual_send_time", "created", "updated")

    def validate(self, attrs: dict) -> dict:
        """
        Validates the Message fields.

        Args:
            attrs (dict): The serializer attributes.

        Returns:
            dict: The validated attributes.

        Raises:
            ValidationError: If the validation fails.
        """
        created = self.context["request"].method == "POST"
        text = attrs.get("text")
        buttons = attrs.get("buttons")
        if created:
            files = attrs.get("files", ())
            validate_files(files)
            validate_files_caption_length(files, text)
            validate_files_count(files)
            validate_file_group_has_no_buttons(files, buttons)
            validate_text_or_attachment_provided(files, text)
        else:
            files = self.instance.attachedfile_set.all()
            validate_files_caption_length(files, text)
            validate_file_group_has_no_buttons(files, buttons)
            validate_text_or_attachment_provided(files, text)
        return attrs

    def create(self, validated_data: dict) -> Message:
        """
        Overrides the default create method of the ModelSerializer to handle the creation
        of a new Message instance with attached files and scheduling of tasks.

        Args:
            validated_data (dict): The validated data.

        Returns:
            Message: The created Message instance.
        """
        files = validated_data.pop("files", ())
        instance = Message.objects.create(**validated_data)
        for attached_file in files:
            attached_file["filename"] = attached_file["file"].name
            attached_file["message"] = instance
            AttachedFile.objects.create(**attached_file)

        if instance.send_time:
            send_message.apply_async((instance.id,), eta=instance.send_time)
        else:
            send_message.delay(instance.id)
        if instance.delete_time:
            delete_message.apply_async((instance.id,), eta=instance.delete_time)

        return instance

    def update(self, instance, validated_data):
        """
        Overrides the default update method of the ModelSerializer to handle the update
        of a Message instance with attached files and scheduling of tasks.

        Args:
            instance (Message): The Message instance to update
            validated_data (dict): The validated data

        Returns:
            Message: The updated Message instance
        """
        instance = super().update(instance, validated_data)

        if validated_data.get("send_time", "key not found") != "key not found":
            if instance.send_time:
                send_message.apply_async((instance.id,), eta=instance.send_time)
            else:
                send_message.delay(instance.id)
        if validated_data.get("delete_time", "key not found") != "key not found":
            if instance.delete_time:
                delete_message.apply_async((instance.id,), eta=instance.delete_time)
        return instance

    def to_internal_value(self, data):
        """
        Overrides the default to_internal_value method of the ModelSerializer to handle the
        conversion of the send_time and delete_time fields for null values.
        """
        if isinstance(data, QueryDict):
            data._mutable = True
        if data.get("send_time") == "null":
            data["send_time"] = None
        if data.get("delete_time") == "null":
            data["delete_time"] = None
        return super().to_internal_value(data)

import logging
from datetime import datetime
from celery import shared_task

from django.utils import timezone
from django.utils.timezone import make_aware

from telebot.apihelper import ApiTelegramException

from core.settings import bot
from .models import Message, AttachedFile
from .utils import TelegramBotUtils
from ..chats.utils import update_migrated_chat_id


@shared_task
def send_message(message_id: int):
    """
    Celery task that sends a message in Telegram using the provided message ID.

    Args:
        message_id (int): The ID of the message in database.
    """
    try:
        message_db = Message.objects.select_related("chat").get(pk=message_id)
    except Message.DoesNotExist:
        logging.warning(f"Tried to send message with ID {message_id} but it was not found.")
        return
    if message_db.is_sent:
        logging.info(f"Tried to send message with ID {message_id} but it is already sent.")
        return
    if message_db.send_time and timezone.now() < message_db.send_time:
        return
    files = AttachedFile.objects.filter(message=message_db).all()
    try:
        sent_message = TelegramBotUtils.send_message(message_db, files)
    except ApiTelegramException as e:
        if e.result_json.get("parameters"):
            if update_migrated_chat_id(message_db.chat, e.result_json["parameters"]):
                send_message.delay(message_id)
                return
            elif e.result_json["parameters"].get("retry_after"):
                sleep_time = e.result_json["parameters"]["retry_after"]
                logging.warning(f"Got error {e.description}. Sleeping for {sleep_time} seconds before retrying.")
                send_message.apply_async(countdown=sleep_time, args=(message_id,))
        else:
            logging.warning(e)
            message_db.tg_sending_error_text = str(e.result_json["description"])[:255]
            message_db.save()
    except Exception as e:
        logging.error(e)
        message_db.tg_sending_error_text = str(e)
        message_db.save()
    else:
        message_db.msg_id = sent_message.message_id
        message_db.actual_send_time = make_aware(datetime.fromtimestamp(sent_message.date))
        message_db.save()


@shared_task
def delete_message(message_id: int):
    """
    Celery task that deletes message from both the chat and database using the provided message ID.

    Args:
        message_id (int): The ID of the message in database.
    """
    try:
        message = Message.objects.get(pk=message_id)
    except Message.DoesNotExist:
        logging.warning(f"Tried to delete message with ID {message_id} but it was not found.")
        return
    if not message.delete_time or timezone.now() < message.delete_time:
        return
    if message.is_sent:
        bot.delete_message(message.chat.tg_id, message.msg_id)
        message.delete()
    else:
        logging.warning(f"Tried to delete message with ID {message_id} but it is not sent yet.")

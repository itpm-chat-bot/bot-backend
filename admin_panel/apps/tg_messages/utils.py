from io import BytesIO
from pathlib import Path
from bleach import clean

from django.core.files.images import ImageFile

from telebot.callback_data import CallbackData
from telebot.types import (
    Message,
    InputMediaPhoto,
    InputMediaVideo,
    InputMediaDocument,
    InlineKeyboardMarkup,
    InlineKeyboardButton,
    Chat,
)

from apps.chats import models as chat_models
from apps.tg_messages.const import FilesTypes
from apps.users.models import UserProfile
from core.settings import bot


class TelegramBotUtils:
    """Utility class for working with Telegram bot."""

    @classmethod
    def update_avatar(cls, instance: UserProfile | chat_models.Chat, tg_chat_data: Chat):
        if tg_chat_data.photo and tg_chat_data.photo.big_file_id != instance.avatar_id:
            file = cls.download_photo_by_id(tg_chat_data.photo.big_file_id)
            instance.avatar = ImageFile(file, name=f"{instance.id}.jpg")
            instance.avatar_id = tg_chat_data.photo.big_file_id
        elif not tg_chat_data.photo:
            instance.avatar = None
            instance.avatar_id = None

    @staticmethod
    def download_photo_by_id(photo_id: str) -> BytesIO:
        """Downloads photo by its id."""
        file_path = bot.get_file(photo_id).file_path
        return BytesIO(bot.download_file(file_path))

    @classmethod
    def send_message(cls, message_db, files) -> Message:
        """
        Sends a message in Telegram based on the provided message database entry.

        Args:
            message_db: The message object from the database.
            files (list[AttachedFile]): The list of attached files from the database for the message.

        Returns:
            Message: The message sent in Telegram.
        """
        text = HTMLUtils.remove_non_telegram_html(message_db.text)
        send_message_kwargs = {
            "chat_id": message_db.chat.tg_id,
            "disable_notification": message_db.disable_notification,
        }
        if message_db.buttons:
            send_message_kwargs = cls._add_buttons(message_db, send_message_kwargs)
        if files:
            message = cls._send_message_with_files(message_db, text, files, send_message_kwargs)
        else:
            message = bot.send_message(
                text=text, **send_message_kwargs, disable_web_page_preview=message_db.disable_web_page_preview
            )
        if message_db.pin_message:
            bot.pin_chat_message(message_db.chat.tg_id, message.message_id, disable_notification=True)
        return message

    @classmethod
    def _send_message_with_files(cls, message_db, text: str, files, send_message_kwargs: dict) -> Message:
        """
        Sends a message with attached files.

        Args:
            message_db: The message object from the database.
            text (str): The text content of the message.
            files (list[AttachedFile]): The list of attached files from the database for the message.
            send_message_kwargs (dict): Additional arguments for sending the message.

        Returns:
            Message: The message sent in Telegram.
        """
        if len(files) > 1:
            media_group = cls._process_files_group(files, text)
            messages = bot.send_media_group(
                message_db.chat.tg_id, media=media_group, disable_notification=message_db.disable_notification
            )
            return messages[0]
        else:
            attached_file = files[0]
            file_io = BytesIO(attached_file.file.read())
            file_io.name = attached_file.filename
            extension = Path(attached_file.filename).suffix[1:].lower()
            if extension in FilesTypes.PICTURES_EXTENSIONS and attached_file.as_media:
                return bot.send_photo(
                    photo=file_io, caption=text, has_spoiler=attached_file.has_spoiler, **send_message_kwargs
                )
            elif extension in FilesTypes.VIDEOS_EXTENSIONS and attached_file.as_media:
                return bot.send_video(
                    video=file_io, caption=text, has_spoiler=attached_file.has_spoiler, **send_message_kwargs
                )
            else:
                return bot.send_document(document=file_io, caption=text, **send_message_kwargs)

    @staticmethod
    def _process_files_group(files, text: str) -> list[InputMediaPhoto | InputMediaVideo | InputMediaDocument]:
        """
        Processes a group of files and creates a list of InputMedia objects for sending as a media group.

        Args:
            files (list[AttachedFile]): The list of attached files from the database for the message.
            text (str): The common text caption for the media group.

        Returns:
            list[InputMedia]: The list of InputMedia objects for the media group.
        """
        media = []
        for attached_file in files:
            file_io = BytesIO(attached_file.file.read())
            file_io.name = attached_file.filename
            extension = Path(attached_file.filename).suffix[1:].lower()
            if extension in FilesTypes.PICTURES_EXTENSIONS and attached_file.as_media:
                media.append(InputMediaPhoto(file_io, has_spoiler=attached_file.has_spoiler))
            elif extension in FilesTypes.VIDEOS_EXTENSIONS and attached_file.as_media:
                media.append(InputMediaVideo(file_io, has_spoiler=attached_file.has_spoiler))
            else:
                media.append(InputMediaDocument(file_io))
        if text:
            media[-1].caption = text
            media[-1].parse_mode = "HTML"
        return media

    @staticmethod
    def _add_buttons(message_db, send_message_kwargs: dict) -> dict:
        """
        Adds inline buttons to the send_message_kwargs dictionary.

        Args:
            message_db: The message object from the database.
            send_message_kwargs (dict): The send_message_kwargs dictionary.

        Returns:
            dict: The send_message_kwargs with added buttons.
        """
        markup = InlineKeyboardMarkup()
        for row in message_db.buttons:
            buttons_row = []
            for btn in row["buttons"]:
                if btn["type"] == "link":
                    button = InlineKeyboardButton(text=btn["name"], url=btn["value"])
                else:
                    hint = CallbackData("message", prefix="hint")
                    button = InlineKeyboardButton(text=btn["name"], callback_data=hint.new(message=btn["value"]))
                buttons_row.append(button)
            markup.add(*buttons_row)
        send_message_kwargs["reply_markup"] = markup
        return send_message_kwargs


class HTMLUtils:
    """Utility class for working with HTML tags."""

    @staticmethod
    def remove_non_telegram_html(text: str) -> str:
        """
        Removes HTML tags and attributes that are not supported by Telegram from the given text.

        Args:
            text (str): The input text containing HTML.

        Returns:
            str: The cleaned text with unsupported HTML tags and attributes removed.
        """
        allowed_tags = {
            "a",
            "b",
            "strong",
            "i",
            "em",
            "u",
            "ins",
            "s",
            "strike",
            "del",
            "tg-spoiler",
            "code",
            "pre",
        }
        allowed_tag_attributes = {"a": "href"}
        allowed_protocols = {"http", "https", "tg"}
        return clean(
            text, tags=allowed_tags, attributes=allowed_tag_attributes, protocols=allowed_protocols, strip=True
        )

    @staticmethod
    def remove_html(text: str) -> str:
        """
        Removes all HTML tags from the given text.

        Args:
            text (str): The input text containing HTML.

        Returns:
            str: The cleaned text with all HTML tags removed.
        """
        return clean(text, tags=[], strip=True)

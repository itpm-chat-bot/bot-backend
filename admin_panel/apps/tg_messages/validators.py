from PIL import Image

from django.core.exceptions import ValidationError
from django.core.validators import URLValidator, FileExtensionValidator
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from .const import (
    BUTTON_TYPES,
    ALLOWED_MESSAGE_TEXT_SIZE,
    ALLOWED_ATTACHMENT_CAPTION_SIZE,
    FilesTypes,
    ALLOWED_PICTURE_SIZE,
    ALLOWED_FILES_COUNT,
    ALLOWED_FILE_SIZE,
)
from .utils import HTMLUtils


def validate_buttons(value: list):
    """
    Validate the buttons structure.

    This function validates the structure of the buttons.
    It checks that the value is a list of button rows, where each row is a list of buttons.
    Each button should have a "name" (str), "type" (str), and "value" (str) attribute.
    The "type" attribute should be one of the available button types defined in `BUTTON_TYPES`.
    If the button type is "link", the "value" should be a valid URL.

    Args:
        value (list): The buttons value to validate.

    Raises:
        ValidationError: If the buttons structure is invalid.
    """
    if value:
        if not type(value) == list:
            raise ValidationError(
                _("Incorrect buttons JSON. It is not an array. Specify array of button rows."),
            )
        for row in value:
            if not type(row) == dict:
                raise ValidationError(
                    _("Incorrect buttons JSON. Every button row should be in the dict."),
                )
            if not row.get("buttons") or type(row["buttons"]) != list:
                raise ValidationError(
                    _("Incorrect buttons JSON. Every button row should have a list of buttons."),
                )
            for btn in row["buttons"]:
                if not btn.get("name") or type(btn["name"]) != str:
                    raise ValidationError(
                        _("%(value)s is not a correct button JSON. Every button shell have a name."),
                        params={"value": str(btn)},
                    )

                if not btn.get("type") or btn["type"] not in BUTTON_TYPES:
                    raise ValidationError(
                        _(
                            "%(value)s is not a correct button JSON. Every button shell have a correct type. "
                            "Available types: " + ", ".join(BUTTON_TYPES)
                        ),
                        params={"value": str(btn)},
                    )

                if not btn.get("value") or type(btn["value"]) != str:
                    raise ValidationError(
                        _("%(value)s is not a correct button JSON. Every button shell have a value."),
                        params={"value": str(btn)},
                    )

                if btn["type"] == "link":
                    try:
                        URLValidator()(btn["value"])
                    except ValidationError:
                        raise ValidationError(
                            _(
                                "%(value)s is not a correct button JSON. "
                                "Button of type link should have a correct URL in its value."
                            ),
                            params={"value": str(btn)},
                        )


def validate_future_date(value):
    """
    Validates that the provided date and time is in the future.

    Args:
        value: The date and time value to validate.

    Raises:
        ValidationError: If the value is not greater than the current time.

    """
    if not value > timezone.now():
        raise ValidationError(_("The time should be in the future."))


def validate_text_size(value):
    """
    Validate the size of a message text.

    This function validates that the length of the message text (after removing HTML tags) does not exceed the allowed limit.
    If the text size exceeds the limit, a validation error is raised.

    Args:
        value: The message text value to validate.

    Raises:
        ValidationError: If the message text size exceeds the allowed limit.

    """
    if len(HTMLUtils.remove_html(value)) > ALLOWED_MESSAGE_TEXT_SIZE:
        raise ValidationError(
            _(f"The message is longer than allowed. Allowed message size is {ALLOWED_MESSAGE_TEXT_SIZE} symbols.")
        )


def validate_text_or_attachment_provided(files, text):
    """
    Validate that the message text or an attachment is provided.

    Args:
        files: The message files.
        text: The message text.

    Raises:
        ValidationError: If the message text or an attachment is not provided.

    """
    if not files and not HTMLUtils.remove_html(text).strip():
        raise ValidationError(_("You should provide either files or text or both."))


def validate_file_group_has_no_buttons(files, buttons):
    """
    Validate that the file group does not contain buttons.

    Args:
        files: The message files.
        buttons: The message buttons.

    Raises:
        ValidationError: If the file group contains buttons.

    """
    if len(files) > 1 and buttons:
        raise ValidationError(
            _("Buttons cannot be added to a group of files. Only one file is allowed in a message with buttons.")
        )


def validate_files_count(files):
    """
    Validate the number of files in a message.

    Args:
        files: The message files.

    Raises:
        ValidationError: If the number of files in a message exceeds the allowed limit.

    """
    if len(files) > ALLOWED_FILES_COUNT:
        raise ValidationError(_(f"You can send up to {ALLOWED_FILES_COUNT} files in a message."))


def validate_files_caption_length(files, text):
    """
    Validate the length of files caption.

    Args:
        files: The message files.
        text: The message text.

    Raises:
        ValidationError: If the length of a file caption exceeds the allowed limit.

    """
    if files and len(HTMLUtils.remove_html(text)) > ALLOWED_ATTACHMENT_CAPTION_SIZE:
        raise ValidationError(_("The attachments caption is longer than allowed 1024 symbols"))


def validate_media(file):
    """
    Validate the file as media.

    This function validates the file as media.
    It checks that the file size does not exceed 10 MB and that the file is an image.

    Args:
        file: The file to validate.

    Raises:
        ValidationError: If the file is not an image or the file size exceeds 10 MB.

    """
    media_format_validator = FileExtensionValidator(
        FilesTypes.PICTURES_EXTENSIONS + FilesTypes.VIDEOS_EXTENSIONS, _("Wrong file format for sending as media")
    )
    media_format_validator(file)
    if file.name.endswith(FilesTypes.PICTURES_EXTENSIONS):
        if file.size > ALLOWED_PICTURE_SIZE:
            raise ValidationError(_("The picture size is too big for sending as media"))
        try:
            Image.open(file).verify()
        except Exception:
            raise ValidationError(_("The file is not an image."))


def validate_file_size(value):
    """
    Validates the size of a file.

    This function checks that the file size does not exceed 50 MB.

    Args:
        value: The file to validate.

    Raises:
        ValidationError: If the file size exceeds 50 MB.

    """
    if value.size > ALLOWED_FILE_SIZE:
        raise ValidationError(_("The file size is too big for sending as media"))


def validate_files(files):
    """
    Validate the files in a message.

    This function validates the files in a message.
    It checks that the documents and media files are not mixed.

    Args:
        files: The message files.

    Raises:
        ValidationError: If the files are sent both as media and as documents.
        ValidationError: If there are media files and the media validation fails.
    """
    has_as_media = False
    has_documents = False
    for attached_file in files:
        if attached_file["as_media"]:
            if has_documents:
                raise ValidationError(_("All files should be sent either as documents or as media"))
            has_as_media = True
            validate_media(attached_file["file"])
        elif has_as_media:
            raise ValidationError(_("All files should be sent either as documents or as media"))
        else:
            has_documents = True


def validate_delete_time_later_than_send_time(send_time, delete_time):
    """
    Validate that the delete time is later than the send time.

    Args:
        send_time: The send time.
        delete_time: The delete time.

    Raises:
        ValidationError: If the delete time is earlier than the send time.

    """
    if send_time and delete_time and delete_time <= send_time:
        raise ValidationError(_("The delete time should be later than the send time."))

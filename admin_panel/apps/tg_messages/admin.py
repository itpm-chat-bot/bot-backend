from django.contrib import admin

from .models import Message, AttachedFile


class AttachedFileAdmin(admin.TabularInline):
    """
    Admin inline interface for the AttachedFile model.

    Displays and configures the admin inline interface for the AttachedFile model within
    the Message admin page in the Django admin site.
    """

    model = AttachedFile


class MessageAdmin(admin.ModelAdmin):
    """
    Admin interface for the Message model.

    Displays and configures the admin interface for the Message model in the Django admin site.
    """

    model = Message
    inlines = [AttachedFileAdmin]
    date_hierarchy = "updated"
    ordering = ("-updated",)
    list_display = ("__str__", "send_time", "delete_time", "updated")
    list_filter = ("send_time", "delete_time")
    search_fields = ("text",)


admin.site.register(Message, MessageAdmin)

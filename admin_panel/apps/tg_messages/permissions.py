from rest_framework.permissions import BasePermission
from rest_framework.request import Request

from apps.chats.models import Chat
from apps.tg_messages.models import Message, AttachedFile


class IsMessageChatAdmin(BasePermission):
    """
    Permission class to restrict access to message creation based on chat admin status.

    This permission class checks if the user is an admin of the chat specified in the message data.
    If the `chat` field is not present in the request data, access is allowed. Otherwise, the user's
    admin status in the chat is verified. Access is granted only if the user is an admin of the chat,
    and denied otherwise.
    """

    def has_permission(self, request, view) -> bool:
        if request.data.get("chat") is None:
            return True
        chats = Chat.objects.filter(admins__user_id=request.user.id, pk=request.data.get("chat")).first()
        return bool(chats)


class IsBotChatAdmin(BasePermission):
    """
    Permission class to restrict access to message creation if the bot is not admin of the chat.

    This permission class checks if the bot is an admin of the chat specified in the message data.
    If the `chat` field is not present in the request data, access is allowed. Otherwise, the bots
    admin status in the chat is verified. Access is granted only if the bot is an admin of the chat
    the message is being sent to, and denied otherwise.
    """

    def has_permission(self, request, view):
        if request.data.get("chat") is None:
            return True
        try:
            return Chat.objects.get(pk=request.data["chat"]).is_bot_admin
        except Chat.DoesNotExist:
            return True


class IsMessageAdmin(BasePermission):
    """
    Permission class to restrict access to message attachments creation based on message admin status.

    This permission class checks if the user is an admin of the message specified in the message data.
    If the `message` field is not present in the request data, access is allowed. Otherwise, the user's
    admin status in the message is verified. Access is granted only if the user is an admin of the message,
    and denied otherwise.
    """

    def has_permission(self, request: Request, view) -> bool:
        message_id = request.data.get("message_id")
        if message_id is None:
            return True
        return Message.objects.filter(pk=message_id, chat__admins__user_id=request.user.id).exists()

    def has_object_permission(self, request: Request, view, obj: AttachedFile) -> bool:
        return Message.objects.filter(pk=obj.message.id, chat__admins__user_id=request.user.id).exists()

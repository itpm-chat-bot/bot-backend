from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.chats.models import Chat
from core.settings import PrivateStorage
from utils.random_filename import RandomFileName
from .const import FilesTypes, ALLOWED_ATTACHMENT_CAPTION_SIZE, ALLOWED_FILES_COUNT
from .validators import (
    validate_buttons,
    validate_future_date,
    validate_text_size,
    validate_delete_time_later_than_send_time,
    validate_media,
    validate_file_size,
)


class Message(models.Model):
    """Stores information about messages sent from the admin panel."""

    msg_id = models.BigIntegerField(_("Telegram message ID"), blank=True, null=True)
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE)
    text = models.TextField(validators=[validate_text_size], blank=True, null=True)
    disable_web_page_preview = models.BooleanField(default=False)
    disable_notification = models.BooleanField(default=False)
    pin_message = models.BooleanField(default=False)
    send_time = models.DateTimeField(blank=True, null=True, validators=[validate_future_date])
    delete_time = models.DateTimeField(blank=True, null=True, validators=[validate_future_date])
    actual_send_time = models.DateTimeField(blank=True, null=True)
    buttons = models.JSONField(blank=True, null=True, validators=[validate_buttons])
    tg_sending_error_text = models.CharField(max_length=255, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    @property
    def is_sent(self):
        return self.msg_id is not None

    def clean(self):
        if self.is_sent:
            raise ValidationError("Message already sent. You can not edit it.")
        validate_delete_time_later_than_send_time(self.send_time, self.delete_time)

    def __str__(self):
        return f"{self.text[:50] if self.text else 'Files'} ({'sent' if self.is_sent else 'unsent'})"


class AttachedFile(models.Model):
    """Stores information about files attached to a message."""

    file = models.FileField(
        upload_to=RandomFileName("attached_files"),
        storage=PrivateStorage(),
        validators=[
            FileExtensionValidator(
                FilesTypes.PICTURES_EXTENSIONS + FilesTypes.VIDEOS_EXTENSIONS + FilesTypes.DOCUMENTS_EXTENSIONS,
                _("The extension is not supported"),
            ),
            validate_file_size,
        ],
    )
    filename = models.CharField(max_length=255)
    has_spoiler = models.BooleanField(default=False)
    as_media = models.BooleanField(default=False)
    message = models.ForeignKey(Message, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def clean(self):
        try:
            message = Message.objects.prefetch_related("attachedfile_set").get(pk=self.message_id)
        except Message.DoesNotExist:
            return
        if message.is_sent:
            raise ValidationError("Message already sent. You can not add files to a sent message.")
        files = message.attachedfile_set.all()
        if message.msg_id:
            raise ValidationError("Message already sent. You can not add files to a sent message.")
        if len(message.text) > ALLOWED_ATTACHMENT_CAPTION_SIZE:
            raise ValidationError("Message text is too long. You can't add files to this message.")
        if message.buttons:
            raise ValidationError("Message must not have buttons.")
        if len(files) >= ALLOWED_FILES_COUNT:
            raise ValidationError("You can not add more than 10 files to one message.")
        if self.as_media:
            validate_media(self.file)
        if bool(files[0].as_media) != self.as_media:
            raise ValidationError(_("All files should be sent either as documents or as media not both."))

    def __str__(self):
        return f"{self.file.name}"

# Generated by Django 4.1.8 on 2023-04-11 16:19

import core.settings.storages
from django.db import migrations, models
import utils.random_filename


class Migration(migrations.Migration):
    dependencies = [
        ("tg_messages", "0002_message_buttons_message_delete_time_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="attachedpicture",
            name="file",
            field=models.ImageField(
                storage=core.settings.storages.PrivateStorage(),
                upload_to=utils.random_filename.RandomFileName("attached_pictures"),
            ),
        ),
    ]

from rest_framework import routers

from .views import MessageViewSet, AttachedFileViewSet

app_name = "messages"

router = routers.SimpleRouter()
router.register("files", AttachedFileViewSet)
router.register("", MessageViewSet)

urlpatterns = router.urls

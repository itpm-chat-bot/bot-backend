from django.utils import timezone
from drf_spectacular.utils import extend_schema, OpenApiParameter, extend_schema_view
from rest_framework import status
from rest_framework.mixins import CreateModelMixin, DestroyModelMixin, UpdateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, GenericViewSet

from .models import Message, AttachedFile
from .permissions import IsMessageChatAdmin, IsMessageAdmin, IsBotChatAdmin
from .serializers import (
    MessageRetrieveSerializer,
    MessageCreateSerializer,
    MessageThinSerializer,
    AttachedFileAddSerializer,
)


@extend_schema_view(
    list=extend_schema(
        description="List of messages created by user.",
        parameters=[
            OpenApiParameter(name="delayed", type=bool, description="Show only delayed messages"),
            OpenApiParameter(name="chat_id", type=int, description="Show messages for specific chat", required=True),
            OpenApiParameter(
                name="sent", type=bool, description="Show only sent messages and messages with sending error"
            ),
        ],
    ),
    retrieve=extend_schema(
        description="Retrieve a message by its id." "" "In files, file_size is the size of the file in kilobytes.",
    ),
)
class MessageViewSet(ModelViewSet):
    """Viewset for performing CRUD operations on messages."""

    queryset = Message.objects.all().order_by("-id")

    def get_queryset(self):
        """
        Queryset of messages created by user.

        This method filters the queryset to show only the messages in the chats where the current user is admin.
        For list of messages, additional filters can be applied:
         - delayed: to retrieve only delayed (unsent) messages
         - sent: to retrieve only sent messages
         - chat_id: to retrieve messages for specific chat

        Returns:
            queryset: The filtered queryset of messages.
        """
        queryset = self.queryset.filter(chat__admins__user_id=self.request.user.id)
        if self.action == "list":
            queryset = queryset.select_related("chat").prefetch_related("attachedfile_set")
            chat_id = self.request.query_params.get("chat_id")
            if chat_id:
                queryset = queryset.filter(chat_id=chat_id)
            delayed = self.request.query_params.get("delayed")
            sent = self.request.query_params.get("sent")
            if delayed:
                queryset = queryset.filter(send_time__isnull=False, send_time__gte=timezone.now()).order_by(
                    "send_time"
                )
            elif sent:
                queryset = queryset.filter(msg_id__isnull=False) | queryset.filter(
                    tg_sending_error_text__isnull=False
                ).order_by("-actual_send_time")
        return queryset

    def get_serializer_class(self):
        """Returns the appropriate serializer class based on the action being performed."""
        if self.action == "retrieve":
            return MessageRetrieveSerializer
        elif self.action == "list":
            return MessageThinSerializer
        else:
            return MessageCreateSerializer

    def get_permissions(self):
        """Returns the appropriate permissions based on the action being performed."""
        if self.action == "create":
            return IsAuthenticated(), IsMessageChatAdmin(), IsBotChatAdmin()
        # we don't need to check permissions for other actions because queryset is already filtered
        else:
            return (IsAuthenticated(),)


class AttachedFileViewSet(GenericViewSet, CreateModelMixin, DestroyModelMixin, UpdateModelMixin):
    """Viewset for performing create and delete operations on attached files."""

    queryset = AttachedFile.objects.all()
    serializer_class = AttachedFileAddSerializer
    permission_classes = [IsAuthenticated, IsMessageAdmin]

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()

        if instance.message.is_sent:
            return Response(
                data={"detail": "The file is attached to the message that has been sent."},
                status=status.HTTP_400_BAD_REQUEST,
            )

        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        if instance.message.is_sent:
            return Response(
                data={"detail": "The file is attached to the message that has been sent."},
                status=status.HTTP_400_BAD_REQUEST,
            )

        return super().update(request, *args, **kwargs)

class FilesTypes:
    """
    File types and their extensions.

    Defines the file types and their corresponding extensions for pictures, videos, and documents.
    """

    PICTURES_EXTENSIONS = ("jpg", "png", "gif", "heic")
    VIDEOS_EXTENSIONS = ("mp4", "m4v")
    DOCUMENTS_EXTENSIONS = (
        "docx",
        "pdf",
        "xlsx",
        "txt",
        "fb2",
        "mobi",
        "zip",
        "rar",
        "epub",
        "pptx",
        "mp3",
        "mov",
        "avi",
        "wmv",
        "webm",
    )


BUTTON_TYPES = ("link", "hint")

ALLOWED_MESSAGE_TEXT_SIZE = 4096
ALLOWED_ATTACHMENT_CAPTION_SIZE = 1024
ALLOWED_PICTURE_SIZE = 10 * 1024 * 1024
ALLOWED_FILE_SIZE = 50 * 1024 * 1024
ALLOWED_FILES_COUNT = 10
MESSAGE_TITLE_LENGTH = 20

from rest_framework.permissions import BasePermission

from apps.users.models import UserProfile


class IsChatAdmin(BasePermission):
    def has_object_permission(self, request, view, obj) -> bool:
        userprofile = UserProfile.objects.get(user=request.user.id)
        return userprofile in obj.admins.all()

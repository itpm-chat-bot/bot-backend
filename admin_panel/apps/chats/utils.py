import logging

from time import sleep

from celery.result import AsyncResult

from django.db import IntegrityError

from telebot.apihelper import ApiTelegramException
from telebot.types import ChatPermissions

from rest_framework.exceptions import ValidationError

from .models import Chat
from apps.tg_messages.utils import TelegramBotUtils
from core.settings import bot


def update_migrated_chat_id(chat: Chat, error_parameters: dict) -> bool:
    """
    Updates chat id if chat has been migrated to another chat id.

    Args:
        chat: Chat instance.
        error_parameters: ApiTelegramException parameters.

    Returns:
        True if chat has been migrated and updated in database. False otherwise.
    """
    if error_parameters.get("migrate_to_chat_id"):
        logging.warning(f"Chat {chat.tg_id} migrated to {error_parameters['migrate_to_chat_id']}. Updating chat id.")
        chat.tg_id = error_parameters["migrate_to_chat_id"]
        try:
            chat.save()
        except IntegrityError:
            logging.warning(f"Chat {chat.tg_id} already exists on migrating chat id. Deleting chat.")
            chat.delete()
            return False
        return True
    return False


def update_chat(chat: Chat):
    """Updates chat info."""
    try:
        chat_tg_data = bot.get_chat(chat.tg_id)
    except ApiTelegramException as e:
        if e.result_json.get("parameters"):
            if update_migrated_chat_id(chat, e.result_json["parameters"]):
                return update_chat(chat)
            elif e.result_json["parameters"].get("retry_after"):
                sleep_time = e.result_json["parameters"]["retry_after"]
                if sleep_time <= 25:
                    logging.warning(
                        f"Retry error: {e.description} | Sleeping for {sleep_time} seconds before retrying."
                    )
                    sleep(sleep_time)
                    return update_chat(chat)
                else:
                    logging.error(f"Retry error: {e.description} | Skipping.")
        else:
            chat.is_bot_admin = False
            chat.save()
            logging.warning(e)
    except Exception as e:
        chat.is_bot_admin = False
        chat.save()
        logging.error(str(e) + f" | Chat id: {chat.tg_id}")
    else:
        chat.username = chat_tg_data.username
        chat.type = chat_tg_data.type
        chat.title = chat_tg_data.title
        chat.description = chat_tg_data.description
        if chat_tg_data.permissions:
            chat.can_send_messages = chat_tg_data.permissions.can_send_messages
            chat.can_send_audios = chat_tg_data.permissions.can_send_audios
            chat.can_send_documents = chat_tg_data.permissions.can_send_documents
            chat.can_send_photos = chat_tg_data.permissions.can_send_photos
            chat.can_send_videos = chat_tg_data.permissions.can_send_videos
            chat.can_send_video_notes = chat_tg_data.permissions.can_send_video_notes
            chat.can_send_voice_notes = chat_tg_data.permissions.can_send_voice_notes
            chat.can_send_polls = chat_tg_data.permissions.can_send_polls
            chat.can_send_other_messages = chat_tg_data.permissions.can_send_other_messages
            chat.can_add_web_page_previews = chat_tg_data.permissions.can_add_web_page_previews
            chat.can_change_info = chat_tg_data.permissions.can_change_info
            chat.can_invite_users = chat_tg_data.permissions.can_invite_users
            chat.can_pin_messages = chat_tg_data.permissions.can_pin_messages
            chat.can_manage_topics = chat_tg_data.permissions.can_manage_topics
        try:
            chat.participants_number = bot.get_chat_member_count(chat.tg_id)
        except ApiTelegramException as e:
            if update_migrated_chat_id(chat, e.result_json["parameters"]):
                chat.participants_number = bot.get_chat_member_count(chat.tg_id)
            else:
                logging.error(f"Error getting chat members count: {e.description} | Chat id: {chat.tg_id}")
        chat.is_bot_admin = True
        TelegramBotUtils.update_avatar(chat, chat_tg_data)
        chat.save()
        sleep(0.1)


def filter_queryset_by_id(queryset, user_id=None, chat_tg_id=None):
    """Filter queryset by user id and chat tg id."""
    if not user_id and not chat_tg_id:
        raise ValidationError(detail="user_id and chat_tg_id parameter is required")
    if user_id and chat_tg_id:
        queryset = queryset.filter(admins__id=user_id, tg_id=chat_tg_id)
    return queryset


def get_current_chat_permissions(instance: Chat) -> ChatPermissions:
    """Use this function to get current ChatPermissions object."""
    permissions = ChatPermissions(
        can_send_messages = instance.can_send_messages,
        can_send_audios = instance.can_send_audios,
        can_send_documents = instance.can_send_documents,
        can_send_photos = instance.can_send_photos,
        can_send_videos = instance.can_send_videos,
        can_send_video_notes = instance.can_send_video_notes,
        can_send_voice_notes = instance.can_send_voice_notes,
        can_send_polls = instance.can_send_polls,
        can_send_other_messages = instance.can_send_other_messages,
        can_add_web_page_previews = instance.can_add_web_page_previews,
        can_change_info = instance.can_change_info,
        can_invite_users = instance.can_invite_users,
        can_pin_messages = instance.can_pin_messages,
        can_manage_topics = instance.can_manage_topics
    )
    return permissions


def get_night_time_chat_permissions(instance: Chat) -> ChatPermissions:
    """Use this function to get ChatPermissions object for night time."""
    permissions = ChatPermissions(
        can_send_messages = instance.can_send_messages_in_night_time,
        can_send_audios = instance.can_send_audios_in_night_time,
        can_send_documents = instance.can_send_documents_in_night_time, 
        can_send_photos = instance.can_send_photos_in_night_time,
        can_send_videos = instance.can_send_videos_in_night_time,
        can_send_video_notes = instance.can_send_video_notes_in_night_time, 
        can_send_voice_notes = instance.can_send_voice_notes_in_night_time,
        can_send_polls = instance.can_send_polls_in_night_time,
        can_send_other_messages = instance.can_send_other_messages_in_night_time
    )
    return permissions


def revoke_celery_task(celery_task_id: str, immediate_revoke: bool = None) -> None:
    """Revoke previosly started celery task."""
    old_task_result = AsyncResult(celery_task_id)
    if immediate_revoke:
        old_task_result.revoke(terminate=True)
        return
    if old_task_result.state not in ["SUCCESS", "REVOKED"]:
        old_task_result.revoke(terminate=True)

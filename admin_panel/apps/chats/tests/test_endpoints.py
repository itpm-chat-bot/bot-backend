from rest_framework import status
from rest_framework.reverse import reverse

from tests.fixtures import *

NEW_CHAT_DATA = {"tg_id": 123, "title": "New chat", "type": "supergroup", "admins": [USER_PROFILE_ID]}
SWITCH_CHATGPT_DATA = {"chat_gpt_activated": True}


def test_list_status_code(bot_client, client, user_client):
    """Tests the status code of the chat-list endpoint for different clients."""
    response = user_client.get(reverse("chats:chat-list"))
    assert response.status_code == status.HTTP_200_OK
    response = bot_client.get(reverse("chats:chat-list"))
    assert response.status_code == status.HTTP_200_OK
    response = client.get(reverse("chats:chat-list"))
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_list_data(bot_client, client, user_client, chats_mock_data):
    """Tests the data returned by the chat-list endpoint for different clients."""
    response = user_client.get(reverse("chats:chat-list"))
    assert CHAT_USER2_ADMIN_ID not in [chat["id"] for chat in response.json()]
    assert CHAT_USER_ADMIN_TITLE in [chat["title"] for chat in response.json()]
    response = bot_client.get(reverse("chats:chat-list"))
    assert len(response.json()) == 3
    assert CHAT_NO_ADMIN_ID in [chat["id"] for chat in response.json()]
    response = client.get(reverse("chats:chat-list"))
    assert type(response.json()) != list


def test_retrieve_status_code(bot_client, client, user_client, chats_mock_data):
    """Tests the status code of the chat-retrieve endpoint for different clients."""
    response = user_client.get(reverse("chats:chat-detail", args=[CHAT_USER_ADMIN_ID]))
    assert response.status_code == status.HTTP_200_OK
    response = user_client.get(reverse("chats:chat-detail", args=[CHAT_USER2_ADMIN_ID]))
    assert response.status_code == status.HTTP_403_FORBIDDEN
    response = user_client.get(reverse("chats:chat-detail", args=[CHAT_NO_ADMIN_ID]))
    assert response.status_code == status.HTTP_403_FORBIDDEN
    response = client.get(reverse("chats:chat-detail", args=[CHAT_USER_ADMIN_ID]))
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    response = bot_client.get(reverse("chats:chat-detail", args=[CHAT_USER_ADMIN_ID]))
    assert response.status_code == status.HTTP_200_OK
    response = bot_client.get(reverse("chats:chat-detail", args=[None]))
    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_retrieve_data(bot_client, client, user_client, chats_mock_data):
    """Tests the data returned by the chat-retrieve endpoint for different clients."""
    response = user_client.get(reverse("chats:chat-detail", args=[CHAT_USER_ADMIN_ID]))
    assert response.json()["id"] == CHAT_USER_ADMIN_ID
    assert response.json()["title"] == CHAT_USER_ADMIN_TITLE
    response = bot_client.get(reverse("chats:chat-detail", args=[CHAT_USER2_ADMIN_ID]))
    assert response.json()["id"] == CHAT_USER2_ADMIN_ID
    assert response.json()["title"] == CHAT_USER2_ADMIN_TITLE
    response = client.get(reverse("chats:chat-detail", args=[CHAT_USER2_ADMIN_ID]))
    with pytest.raises(KeyError):
        assert not response.json()["id"]


def test_create_status_code(bot_client, client, user_client, chats_mock_data):
    """Tests the status code of the chat-create endpoint for different clients."""
    response = user_client.post(reverse("chats:chat-list"), data=NEW_CHAT_DATA)
    assert response.status_code == status.HTTP_403_FORBIDDEN
    response = bot_client.post(reverse("chats:chat-list"), data=NEW_CHAT_DATA)
    assert response.status_code == status.HTTP_201_CREATED
    response = bot_client.get(reverse("chats:chat-detail", args=[response.json()["id"]]))
    assert response.status_code == status.HTTP_200_OK
    response = user_client.get(reverse("chats:chat-detail", args=[response.json()["id"]]))
    assert response.status_code == status.HTTP_200_OK


def test_create_data(bot_client, client, user_client, chats_mock_data):
    """Tests the data returned by the chat-create endpoint for different clients."""
    response = bot_client.post(reverse("chats:chat-list"), data=NEW_CHAT_DATA)
    response = bot_client.get(reverse("chats:chat-detail", args=[response.json()["id"]]))
    assert response.json()["tg_id"] == NEW_CHAT_DATA["tg_id"]
    assert response.json()["title"] == NEW_CHAT_DATA["title"]


def test_update_status_code(bot_client, client, user_client, chats_mock_data):
    """Tests the status code of the chat-update endpoint for different clients."""
    response = user_client.patch(
        reverse("chats:chat-detail", args=[CHAT_USER_ADMIN_ID]) + "switch_chatgpt/", data=SWITCH_CHATGPT_DATA
    )
    assert response.status_code == status.HTTP_200_OK
    response = user_client.patch(
        reverse("chats:chat-detail", args=[CHAT_USER2_ADMIN_ID]) + "switch_chatgpt/", data=SWITCH_CHATGPT_DATA
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN
    response = bot_client.patch(
        reverse("chats:chat-detail", args=[CHAT_USER2_ADMIN_ID]) + "switch_chatgpt/", data=SWITCH_CHATGPT_DATA
    )
    assert response.status_code == status.HTTP_200_OK
    response = user_client.patch(reverse("chats:chat-detail", args=[CHAT_USER_ADMIN_ID]), data=NEW_CHAT_DATA)
    assert response.status_code == status.HTTP_403_FORBIDDEN
    response = client.patch(reverse("chats:chat-detail", args=[CHAT_USER_ADMIN_ID]), data=NEW_CHAT_DATA)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    response = bot_client.patch(reverse("chats:chat-detail", args=[CHAT_USER_ADMIN_ID]), data=NEW_CHAT_DATA)
    assert response.status_code == status.HTTP_200_OK


def test_update_data(bot_client, client, user_client, chats_mock_data):
    """Tests the data returned by the chat-update endpoint for different clients."""
    response = user_client.patch(reverse("chats:chat-detail", args=[CHAT_USER_ADMIN_ID]), data=NEW_CHAT_DATA)
    with pytest.raises(KeyError):
        assert not response.json()["id"]
    response = user_client.patch(
        reverse("chats:chat-detail", args=[CHAT_USER_ADMIN_ID]) + "switch_chatgpt/", data=SWITCH_CHATGPT_DATA
    )
    assert response.json()["chat_gpt_activated"] == SWITCH_CHATGPT_DATA["chat_gpt_activated"]
    response = bot_client.patch(reverse("chats:chat-detail", args=[CHAT_USER_ADMIN_ID]), data=NEW_CHAT_DATA)
    response = bot_client.get(reverse("chats:chat-detail", args=[response.json()["id"]]))
    assert response.json()["tg_id"] == NEW_CHAT_DATA["tg_id"]
    assert response.json()["title"] == NEW_CHAT_DATA["title"]


def test_delete_status_code(bot_client, client, user_client, chats_mock_data):
    """Tests the status code of the chat-delete endpoint for different clients."""
    response = client.delete(reverse("chats:chat-detail", args=[CHAT_USER_ADMIN_ID]))
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    response = user_client.delete(reverse("chats:chat-detail", args=[CHAT_USER_ADMIN_ID]))
    assert response.status_code == status.HTTP_204_NO_CONTENT
    response = bot_client.delete(reverse("chats:chat-detail", args=[CHAT_USER_ADMIN_ID]))
    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_delete_data(bot_client, client, user_client, chats_mock_data):
    """Tests the data returned by the chat-delete endpoint for different clients."""
    response = user_client.delete(reverse("chats:chat-detail", args=[CHAT_USER_ADMIN_ID]))
    assert response.content.decode() == ""

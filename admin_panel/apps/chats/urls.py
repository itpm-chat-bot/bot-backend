from rest_framework import routers

from apps.chats.views import ChatViewSet

app_name = "chats"

router = routers.SimpleRouter()
router.register("", ChatViewSet)

urlpatterns = router.urls

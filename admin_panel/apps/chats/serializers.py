from rest_framework.serializers import ModelSerializer
from rest_framework import serializers

from .models import Chat


class ChatSerializer(ModelSerializer):
    """The serializer responsible for creating, editing and getting chat data for bot."""

    class Meta:
        model = Chat
        fields = (
            "id",
            "tg_id",
            "username",
            "type",
            "title",
            "description",
            "invite_link",
            "avatar",
            "avatar_id",
            "participants_number",
            "timezone",
            "main_admin",
            "admins",
            "users",
            "chat_gpt_activated",
            "is_bot_admin",
            "should_remove_service_message_new_chat_members",
            "should_remove_service_message_left_chat_members",
            "should_remove_service_message_new_chat_photo",
            "should_remove_service_message_new_chat_title",
            "should_remove_service_message_removed_chat_photo",
            "can_send_messages",
            "can_send_audios",
            "can_send_documents",
            "can_send_photos",
            "can_send_videos",
            "can_send_video_notes",
            "can_send_voice_notes",
            "can_send_polls",
            "can_send_other_messages",
            "can_add_web_page_previews",
            "can_change_info",
            "can_invite_users",
            "can_pin_messages",
            "can_manage_topics",
            "welcome_trigger_on",
            "goodbye_trigger_on",
            "welcome_message",
            "goodbye_message",
            "delete_msg_after_seconds",
            "night_time_on",
            "start_night_time",
            "end_night_time",
            "notification_time",
            "notification_text",
            "notification_celery_task_id",
            "start_night_time_celery_task_id",
            "end_night_time_celery_task_id",
            "can_send_messages_in_night_time",
            "can_send_audios_in_night_time",
            "can_send_documents_in_night_time",
            "can_send_photos_in_night_time",
            "can_send_videos_in_night_time",
            "can_send_video_notes_in_night_time",
            "can_send_voice_notes_in_night_time",
            "can_send_polls_in_night_time",
            "can_send_other_messages_in_night_time",
            "duration_restriction",
            "restricted_chat_member_can_send_messages",
            "restricted_chat_member_can_send_audios",
            "restricted_chat_member_can_send_documents",
            "restricted_chat_member_can_send_photos",
            "restricted_chat_member_can_send_videos",
            "restricted_chat_member_can_send_video_notes",
            "restricted_chat_member_can_send_voice_notes",
            "restricted_chat_member_can_send_polls",
            "restricted_chat_member_can_send_other_messages",
            "restricted_chat_member_can_add_web_page_previews",
            "restricted_chat_member_can_change_info",
            "restricted_chat_member_can_invite_users",
            "restricted_chat_member_can_pin_messages",
            "restricted_chat_member_can_manage_topics",
        )


class ChatUserSerializer(ModelSerializer):
    """The serializer responsible for getting chat data for user."""

    class Meta:
        model = Chat
        fields = (
            "id",
            "tg_id",
            "username",
            "type",
            "title",
            "description",
            "invite_link",
            "avatar",
            "participants_number",
            "timezone",
            "chat_gpt_activated",
            "is_bot_admin",
            "should_remove_service_message_new_chat_members",
            "should_remove_service_message_left_chat_members",
            "should_remove_service_message_new_chat_photo",
            "should_remove_service_message_new_chat_title",
            "should_remove_service_message_removed_chat_photo",
            "can_send_messages",
            "can_send_audios",
            "can_send_documents",
            "can_send_photos",
            "can_send_videos",
            "can_send_video_notes",
            "can_send_voice_notes",
            "can_send_polls",
            "can_send_other_messages",
            "can_add_web_page_previews",
            "can_change_info",
            "can_invite_users",
            "can_pin_messages",
            "can_manage_topics",
            "welcome_trigger_on",
            "goodbye_trigger_on",
            "delete_msg_after_seconds",
            "main_admin",
            "welcome_message",
            "goodbye_message",
            "night_time_on",
            "start_night_time",
            "end_night_time",
            "notification_time",
            "notification_text",
            "notification_celery_task_id",
            "start_night_time_celery_task_id",
            "end_night_time_celery_task_id",
            "can_send_messages_in_night_time",
            "can_send_audios_in_night_time",
            "can_send_documents_in_night_time",
            "can_send_photos_in_night_time",
            "can_send_videos_in_night_time",
            "can_send_video_notes_in_night_time",
            "can_send_voice_notes_in_night_time",
            "can_send_polls_in_night_time",
            "can_send_other_messages_in_night_time",
            "duration_restriction",
            "restricted_chat_member_can_send_messages",
            "restricted_chat_member_can_send_audios",
            "restricted_chat_member_can_send_documents",
            "restricted_chat_member_can_send_photos",
            "restricted_chat_member_can_send_videos",
            "restricted_chat_member_can_send_video_notes",
            "restricted_chat_member_can_send_voice_notes",
            "restricted_chat_member_can_send_polls",
            "restricted_chat_member_can_send_other_messages",
            "restricted_chat_member_can_add_web_page_previews",
            "restricted_chat_member_can_change_info",
            "restricted_chat_member_can_invite_users",
            "restricted_chat_member_can_pin_messages",
            "restricted_chat_member_can_manage_topics",
        )


class ChatThinSerializer(ModelSerializer):
    """The serializer responsible for getting a list of chats for user."""

    class Meta:
        model = Chat
        fields = (
            "id",
            "tg_id",
            "type",
            "title",
            "avatar",
            "participants_number",
            "timezone",
            "is_bot_admin",
        )


class SwitchChatGPTSerializer(ModelSerializer):
    """The serializer responsible for switching ChatGPT status for the chat."""

    class Meta:
        model = Chat
        fields = ("chat_gpt_activated",)


class EditTimezoneSerializer(ModelSerializer):
    """The serializer responsible for editing chat timezone."""

    class Meta:
        model = Chat
        fields = ("timezone",)


class EditChatServiceMessageSerializer(ModelSerializer):
    """The serializer responsible for editing chat service messages settings."""

    class Meta:
        model = Chat
        fields = (
            "should_remove_service_message_new_chat_members",
            "should_remove_service_message_left_chat_members",
            "should_remove_service_message_new_chat_title",
            "should_remove_service_message_new_chat_photo",
            "should_remove_service_message_removed_chat_photo",
        )


class EditChatPermissionsSerializer(ModelSerializer):
    """The serializer responsible for getting and editing chat permissions."""

    class Meta:
        model = Chat
        fields = (
            "can_send_messages",
            "can_send_audios",
            "can_send_documents",
            "can_send_photos",
            "can_send_videos",
            "can_send_video_notes",
            "can_send_voice_notes",
            "can_send_polls",
            "can_send_other_messages",
            "can_add_web_page_previews",
            "can_change_info",
            "can_invite_users",
            "can_pin_messages",
            "can_manage_topics",
        )


class EditChatNightTimeSerializer(ModelSerializer):
    """The serializer responsible for editing chat night time settings."""

    class Meta:
        model = Chat
        fields = (
            "timezone",
            "night_time_on",
            "start_night_time",
            "end_night_time",
            "notification_time",
            "notification_text",
            "can_send_messages_in_night_time",
            "can_send_audios_in_night_time",
            "can_send_documents_in_night_time",
            "can_send_photos_in_night_time",
            "can_send_videos_in_night_time",
            "can_send_video_notes_in_night_time",
            "can_send_voice_notes_in_night_time",
            "can_send_polls_in_night_time",
            "can_send_other_messages_in_night_time",
        )


class EditChatPermissionsForRestrictedChatMembersSerializer(ModelSerializer):
    """The serializer responsible for getting and editing chat permissions for restricted chat members."""

    class Meta:
        model = Chat
        fields = (
            "duration_restriction",
            "restricted_chat_member_can_send_messages",
            "restricted_chat_member_can_send_audios",
            "restricted_chat_member_can_send_documents",
            "restricted_chat_member_can_send_photos",
            "restricted_chat_member_can_send_videos",
            "restricted_chat_member_can_send_video_notes",
            "restricted_chat_member_can_send_voice_notes",
            "restricted_chat_member_can_send_polls",
            "restricted_chat_member_can_send_other_messages",
            "restricted_chat_member_can_add_web_page_previews",
            "restricted_chat_member_can_change_info",
            "restricted_chat_member_can_invite_users",
            "restricted_chat_member_can_pin_messages",
            "restricted_chat_member_can_manage_topics",
        )


class GetChatParticipantsNumberSerializer(ModelSerializer):
    """The serializer responsible for getting the number of chat participants."""

    class Meta:
        model = Chat
        fields = ("participants_number",)


class ActiveUsersCountSerializer(serializers.Serializer):
    general_number_of_active_users = serializers.IntegerField()
    number_of_active_users_for_last24 = serializers.IntegerField()


class GetChatTriggerAllowanceSerializer(ModelSerializer):
    """The serializer responsible for getting the right of using triggers."""

    class Meta:
        model = Chat
        fields = (
            "welcome_trigger_on",
            "goodbye_trigger_on",
        )


class EditMessagesForGreetingAndGoodbyeSerializer(ModelSerializer):
    """The serializer responsible for getting and editing messages for greeting and goodbye."""

    class Meta:
        model = Chat
        fields = (
            "welcome_message",
            "goodbye_message",
            "delete_msg_after_seconds",
        )


class GetChatAgeSerializer(ModelSerializer):
    """The serializer responsible for getting age of the chat."""

    class Meta:
        model = Chat
        fields = ("created",)

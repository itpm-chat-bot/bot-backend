from datetime import timedelta
from datetime import datetime

from dateutil.relativedelta import relativedelta

from django.db.models import Q
from django.utils import timezone
from drf_spectacular.utils import extend_schema
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .models import Chat
from .permissions import IsChatAdmin
from .serializers import (
    ChatSerializer,
    ChatUserSerializer,
    ChatThinSerializer,
    SwitchChatGPTSerializer,
    EditTimezoneSerializer,
    EditChatServiceMessageSerializer,
    GetChatParticipantsNumberSerializer,
    ActiveUsersCountSerializer,
    GetChatAgeSerializer,
    GetChatTriggerAllowanceSerializer,
    EditMessagesForGreetingAndGoodbyeSerializer,
    EditChatNightTimeSerializer,
    EditChatPermissionsSerializer,
    EditChatPermissionsForRestrictedChatMembersSerializer,
)
from .utils import filter_queryset_by_id
from ..participants.models import Participant
from ..users.permissions import IsTelegramBot


class ChatViewSet(ModelViewSet):
    """The viewset responsible for creating, editing and getting chat data by the bot."""

    queryset = Chat.objects.all().order_by("-id")
    serializer_class = ChatSerializer

    def get_permissions(self) -> list:
        """
        Instantiates and returns the list of permissions that this view requires.

        Permissions:
            - user or bot: for retrieve, list, switch_chatgpt, update_timezone actions, update_service_messages.
            - only bot: for all other actions (create, update, destroy).
        """
        if self.action in (
            "retrieve",
            "list",
            "switch_chatgpt",
            "update_timezone",
            "update_service_messages",
            "destroy",
            "get_service_messages_settings",
            "get_night_time_settings",
            "update_night_time_settings",
            "get_chat_permissions",
            "update_chat_permissions",
            "get_chat_permissions_for_restricted_chat_members",
            "update_chat_permissions_for_restricted_chat_members",
            "get_chat_participants_number",
            "get_chat_description",
            "users_count",
            "age_count",
            "get_messages_for_greeting_and_goodbye",
            "update_messages_for_greeting_and_goodbye",
        ):
            permission_classes = [(IsAuthenticated & IsChatAdmin) | IsTelegramBot]
        else:
            permission_classes = [IsTelegramBot]
        return [permission() for permission in permission_classes]

    def get_object(self):
        """
        Returns the object the view is displaying.

        Changes the lookup_field to tg_id for bot compatibility for some actions.
        """
        if self.action in (
            "partial_update",
            "update",
            "get_service_messages_settings",
            "get_chat_participants_number",
            "update_chat_permissions",
            "get_chat_permissions_for_restricted_chat_members",
            "get_chat_trigger_allowance",
        ):
            self.lookup_field = "tg_id"
            self.lookup_url_kwarg = "pk"
        return super().get_object()

    def retrieve(self, request: Request, *args, **kwargs):
        """
        Returns a Chat instance using ChatUserSerializer for users and ChatSerializer for the bot.
        Permissions: Any instance for the bot and only if the user is in the chat admins list for users.
        """
        instance = self.get_object()
        if request.user.is_authenticated:
            serializer = ChatUserSerializer(instance)
        else:
            serializer = self.get_serializer(instance)
        return Response(serializer.data)

    @extend_schema(
        responses={200: ChatThinSerializer, 403: str, 404: str},
    )
    def list(self, request: Request, *args, **kwargs):
        """
        Returns list of Chat instances.
        It uses ChatThinSerializer for users and ChatSerializer for the bot.

        For the bot, user_id and chat_tg_id query parameters can be used to filter chats by user and chat.
        """
        queryset = self.filter_queryset(self.get_queryset())
        if request.user.is_authenticated:
            queryset = queryset.filter(admins__user_id=request.user.id).distinct()
            serializer = ChatThinSerializer(queryset, many=True)
        else:
            user_id = self.request.query_params.get("user_id")
            chat_tg_id = self.request.query_params.get("chat_tg_id")
            queryset = filter_queryset_by_id(
                queryset,
                user_id=user_id,
                chat_tg_id=chat_tg_id
            )
            serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(["PATCH"], detail=True)
    def switch_chatgpt(self, request: Request, *args, **kwargs):
        """Edits chat_gpt_activated attribute of the chat instance."""
        instance = self.get_object()
        serializer = SwitchChatGPTSerializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

    @action(["PATCH"], detail=True)
    def update_timezone(self, request: Request, *args, **kwargs):
        """Edits timezone attribute of the chat instance."""
        instance = self.get_object()
        serializer = EditTimezoneSerializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

    @extend_schema(
        request=EditChatServiceMessageSerializer,
        responses={200: EditChatServiceMessageSerializer, 403: str, 404: str},
    )
    @action(["GET"], detail=True)
    def get_service_messages_settings(self, request: Request, *args, **kwargs):
        """Provides the ability to get the service message settings of the chat instance."""
        instance = self.get_object()
        serializer = EditChatServiceMessageSerializer(instance)
        return Response(serializer.data)

    @extend_schema(
        request=EditChatServiceMessageSerializer,
        responses={200: EditChatServiceMessageSerializer, 403: str, 404: str},
    )
    @action(["PATCH"], detail=True)
    def update_service_messages(self, request: Request, *args, **kwargs):
        """Provides the ability to edit the service message settings of the chat instance."""
        instance = self.get_object()
        serializer = EditChatServiceMessageSerializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)
    
    @extend_schema(
        request=EditChatNightTimeSerializer,
        responses={200: EditChatNightTimeSerializer, 403: str, 404: str},
    )
    @action(["GET"], detail=True)
    def get_night_time_settings(self, request: Request, *args, **kwargs):
        """Provides the ability to get the chat night time settings of the chat instance."""
        instance = self.get_object()
        serializer = EditChatNightTimeSerializer(instance)
        return Response(serializer.data)

    @extend_schema(
        request=EditChatNightTimeSerializer,
        responses={200: EditChatNightTimeSerializer, 403: str, 404: str},
    )
    @action(["PATCH"], detail=True)
    def update_night_time_settings(self, request: Request, *args, **kwargs):
        """Provides the ability to edit the chat night time settings of the chat instance."""
        instance = self.get_object()
        serializer = EditChatNightTimeSerializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)
    
    @extend_schema(
        request=EditChatPermissionsSerializer,
        responses={200: EditChatPermissionsSerializer, 403: str, 404: str},
    )
    @action(["GET"], detail=True)
    def get_chat_permissions(self, request: Request, *args, **kwargs):
        """Provides the ability to get the chat permissions of the chat instance."""
        instance = self.get_object()
        serializer = EditChatPermissionsSerializer(instance)
        return Response(serializer.data)
    
    @extend_schema(
        request=EditChatPermissionsSerializer,
        responses={200: EditChatPermissionsSerializer, 403: str, 404: str},
    )
    @action(["PATCH"], detail=True)
    def update_chat_permissions(self, request: Request, *args, **kwargs):
        """Provides the ability to edit the chat permissions of the chat instance."""
        instance = self.get_object()
        serializer = EditChatPermissionsSerializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)
    
    @extend_schema(
        request=EditChatPermissionsForRestrictedChatMembersSerializer,
        responses={200: EditChatPermissionsForRestrictedChatMembersSerializer, 403: str, 404: str},
    )
    @action(["GET"], detail=True)
    def get_chat_permissions_for_restricted_chat_members(self, request: Request, *args, **kwargs):
        """Provides the ability to get the chat permissions for restricted chat members of the chat instance."""
        instance = self.get_object()
        serializer = EditChatPermissionsForRestrictedChatMembersSerializer(instance)
        return Response(serializer.data)
    
    @extend_schema(
        request=EditChatPermissionsForRestrictedChatMembersSerializer,
        responses={200: EditChatPermissionsForRestrictedChatMembersSerializer, 403: str, 404: str},
    )
    @action(["PATCH"], detail=True)
    def update_chat_permissions_for_restricted_chat_members(self, request: Request, *args, **kwargs):
        """Provides the ability to edit the chat permissions for restricted chat members of the chat instance."""
        instance = self.get_object()
        serializer = EditChatPermissionsForRestrictedChatMembersSerializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

    @extend_schema(
        request=GetChatParticipantsNumberSerializer,
        responses={200: GetChatParticipantsNumberSerializer, 403: str, 404: str},
    )
    @action(["GET"], detail=True)
    def get_chat_participants_number(self, request: Request, *args, **kwargs):
        """Provides the ability to get the number of chat participants of the chat instance."""
        instance = self.get_object()
        serializer = GetChatParticipantsNumberSerializer(instance)
        return Response(serializer.data)

    @extend_schema(
        request=ActiveUsersCountSerializer,
        responses={200: ActiveUsersCountSerializer, 403: str, 404: str},
    )
    @action(["GET"], detail=True)
    def users_count(self, request: Request, *args, **kwargs):
        """
        Returns number of general active users and active users for the last 24 hours.
        """
        instance = self.get_object()
        chat_tg_id = instance.tg_id
        queryset = Participant.objects.all()
        participants = queryset.filter(chat__tg_id=chat_tg_id)
        active_users_count = participants.filter(
            Q(number_messages__gt=0) | Q(number_reactions__gt=0) | Q(number_files__gt=0) | Q(number_links__gt=0)
        ).count()
        current_time = timezone.now()
        last_accepted_time = current_time - timedelta(hours=24)
        active_last_24hours_users = participants.filter(
            last_activity__range=(last_accepted_time, current_time)
        ).count()
        return Response(
            {
                "general_number_of_active_users": active_users_count,
                "active_last_24hours_users": active_last_24hours_users,
            }
        )
    
    @extend_schema(
        request=GetChatAgeSerializer,
        responses={200: GetChatAgeSerializer, 403: str, 404: str},
    )
    @action(["GET"], detail=True)
    def age_count(self, request: Request, *args, **kwargs):
        """
        Returns age of chat.
        """
        instance = self.get_object()
        now = datetime.now(instance.created.tzinfo)
        delta = relativedelta(now, instance.created)
        total_days = (now - instance.created).days
        age = {
            "years": delta.years,
            "months": delta.months,
            "days": delta.days,
            "total_days": total_days
        }
        return Response(age)
    
    @extend_schema(
        request=GetChatTriggerAllowanceSerializer,
        responses={200: GetChatTriggerAllowanceSerializer, 403: str, 404: str},
    )
    @action(["GET"], detail=True)
    def get_chat_trigger_allowance(self, request: Request, *args, **kwargs):
        """Provides the ability to get the trigger allowance settings of the chat instance."""
        instance = self.get_object()
        serializer = GetChatTriggerAllowanceSerializer(instance)
        return Response(serializer.data)

    @extend_schema(
        request=EditMessagesForGreetingAndGoodbyeSerializer,
        responses={200: EditMessagesForGreetingAndGoodbyeSerializer, 403: str, 404: str},
    )
    @action(["GET"], detail=True)
    def get_messages_for_greeting_and_goodbye(self, request: Request, *args, **kwargs):
        """Returns the hello and goodbye message for this chat."""
        instance = self.get_object()
        serializer = EditMessagesForGreetingAndGoodbyeSerializer(instance)
        return Response(serializer.data)
    
    @extend_schema(
        request=EditMessagesForGreetingAndGoodbyeSerializer,
        responses={200: EditMessagesForGreetingAndGoodbyeSerializer, 403: str, 404: str},
    )
    @action(["PATCH"], detail=True)
    def update_messages_for_greeting_and_goodbye(self, request: Request, *args, **kwargs):
        """Provides the ability to edit the message for greeting and goodbye of the chat instance."""
        instance = self.get_object()
        serializer = EditMessagesForGreetingAndGoodbyeSerializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

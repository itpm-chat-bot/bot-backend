# Generated by Django 4.2.4 on 2025-01-14 18:41

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("chats", "0027_rename_celery_task_id_chat_notification_celery_task_id"),
    ]

    operations = [
        migrations.AddField(
            model_name="chat",
            name="end_night_time_celery_task_id",
            field=models.CharField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="chat",
            name="start_night_time_celery_task_id",
            field=models.CharField(blank=True, null=True),
        ),
    ]

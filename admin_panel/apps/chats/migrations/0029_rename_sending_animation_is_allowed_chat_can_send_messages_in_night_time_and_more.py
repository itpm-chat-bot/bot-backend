# Generated by Django 4.2.4 on 2025-01-16 10:10

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("chats", "0028_chat_end_night_time_celery_task_id_and_more"),
    ]

    operations = [
        migrations.RenameField(
            model_name="chat",
            old_name="sending_animation_is_allowed",
            new_name="can_send_messages_in_night_time",
        ),
        migrations.RemoveField(
            model_name="chat",
            name="sending_audio_is_allowed",
        ),
        migrations.RemoveField(
            model_name="chat",
            name="sending_document_is_allowed",
        ),
        migrations.RemoveField(
            model_name="chat",
            name="sending_photo_is_allowed",
        ),
        migrations.RemoveField(
            model_name="chat",
            name="sending_sticker_is_allowed",
        ),
        migrations.RemoveField(
            model_name="chat",
            name="sending_text_is_allowed",
        ),
        migrations.RemoveField(
            model_name="chat",
            name="sending_video_is_allowed",
        ),
        migrations.RemoveField(
            model_name="chat",
            name="sending_video_note_is_allowed",
        ),
        migrations.RemoveField(
            model_name="chat",
            name="sending_voice_is_allowed",
        ),
        migrations.AddField(
            model_name="chat",
            name="can_send_audios_in_night_time",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="chat",
            name="can_send_documents_in_night_time",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="chat",
            name="can_send_other_messages_in_night_time",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="chat",
            name="can_send_photos_in_night_time",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="chat",
            name="can_send_polls_in_night_time",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="chat",
            name="can_send_video_notes_in_night_time",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="chat",
            name="can_send_videos_in_night_time",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="chat",
            name="can_send_voice_notes_in_night_time",
            field=models.BooleanField(default=False),
        ),
    ]

# Generated by Django 4.2.4 on 2024-11-28 11:55

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("chats", "0024_chat_can_add_web_page_previews_chat_can_change_info_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="chat",
            name="celery_task_id",
            field=models.CharField(blank=True, null=True),
        ),
    ]

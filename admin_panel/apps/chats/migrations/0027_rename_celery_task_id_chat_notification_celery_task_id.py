# Generated by Django 4.2.4 on 2025-01-11 15:06

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("chats", "0026_rename_end_time_chat_end_night_time_and_more"),
    ]

    operations = [
        migrations.RenameField(
            model_name="chat",
            old_name="celery_task_id",
            new_name="notification_celery_task_id",
        ),
    ]

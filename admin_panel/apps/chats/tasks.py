import logging
import pytz

from datetime import datetime, timedelta
from time import sleep

from celery import shared_task

from django.utils import timezone

from .models import Chat
from .utils import update_chat, get_current_chat_permissions, get_night_time_chat_permissions, revoke_celery_task
from apps.participants.models import Participant, ChatUsersStatistics
from core.settings import bot


@shared_task
def update_all_chats():
    """Celery task that updates all chats info."""
    day_ago = timezone.now() - timezone.timedelta(days=1)
    all_chats = Chat.objects.filter(is_bot_admin=True) | Chat.objects.filter(is_bot_admin=False, updated__lte=day_ago)
    for chat in all_chats:
        try:
            update_chat(chat)
        except Exception as e:
            logging.error(str(e) + f" | Chat id: {chat.tg_id}")


@shared_task
def update_chat_users_statistics():
    """Celery task that updates chat users statistics."""
    current_time = timezone.now()
    last_accepted_time = current_time - timedelta(hours=24)

    chats = Chat.objects.all()

    for chat in chats:
        participants = Participant.objects.filter(chat=chat)
        total_number_of_users = participants.count()
        active_users = participants.filter(last_activity__range=(last_accepted_time, current_time))
        number_of_active_users_for_last24hours = active_users.count()

        ChatUsersStatistics.objects.update_or_create(
            chat=chat,
            date=current_time.date(),
            defaults={
                "total_number_of_users": total_number_of_users,
                "number_of_active_users_for_last24hours": number_of_active_users_for_last24hours,
            },
        )


@shared_task
def restart_night_time_in_chats():
    """Celery task that restarts night time in all chats every day at midnight."""
    chats = Chat.objects.filter(night_time_on=True)
    for chat in chats:
        chat_timezone = pytz.timezone(chat.timezone)
        current_time = datetime.now(chat_timezone)
        # Create notification sending time
        notification_sending_time = datetime.combine(current_time.date(), chat.start_night_time) - timedelta(seconds=chat.notification_time)
        notification_sending_time = chat_timezone.localize(notification_sending_time)
        # Create a time at which the night time chat starts
        start_night_time = datetime.combine(current_time.date(), chat.start_night_time)
        start_night_time = chat_timezone.localize(start_night_time)
        # Create a time at which the night time chat ends
        end_night_time = datetime.combine(current_time.date(), chat.end_night_time)
        end_night_time = chat_timezone.localize(end_night_time)
        if end_night_time < start_night_time:
            end_night_time += timedelta(days=1)
        
        # Revoke celery tasks if they exist
        if chat.notification_celery_task_id:
            revoke_celery_task(chat.notification_celery_task_id, immediate_revoke=True)
        if chat.start_night_time_celery_task_id:
            revoke_celery_task(chat.start_night_time_celery_task_id, immediate_revoke=True)
        if chat.end_night_time_celery_task_id:
            revoke_celery_task(chat.end_night_time_celery_task_id, immediate_revoke=True)

        # Run celery tasks
        notification_task = send_notification_about_night_time.apply_async((chat.tg_id, chat.notification_text), eta=notification_sending_time)
        start_night_time_task = set_chat_permissions.apply_async((chat.tg_id, "night"), eta=start_night_time)
        end_night_time_task = set_chat_permissions.apply_async((chat.tg_id, "current"), eta=end_night_time)
        chat.notification_celery_task_id = notification_task.id
        chat.start_night_time_celery_task_id = start_night_time_task.id
        chat.end_night_time_celery_task_id = end_night_time_task.id
        chat.save()
        sleep(0.1)


@shared_task
def send_notification_about_night_time(chat_id: int, notification_text: str):
    """
    Celery task that sends a message (notification) in chat about the beginning of night time.
    
    Args:
        tg_id (int): The ID of the chat in database.
        notification_text (str): The text of notification about the beginning of night time in chat.
    """
    try:
        bot.send_message(chat_id=chat_id, text=notification_text)
    except Exception as e:
        logging.error(e)


@shared_task
def set_chat_permissions(chat_id: int, chat_permissions_type: str = None):
    """
    Celery task that set chat permissions in chat for all chat members (except the creater and
    administators)."""
    try:
        instance = Chat.objects.get(tg_id=chat_id)
        if chat_permissions_type == "night":
            chat_permissions = get_night_time_chat_permissions(instance)
        elif chat_permissions_type == "current":
            chat_permissions = get_current_chat_permissions(instance)
        
        bot.set_chat_permissions(chat_id=chat_id, permissions=chat_permissions)
    except Exception as e:
        logging.error(e)

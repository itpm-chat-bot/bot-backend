import logging
import pytz

from datetime import datetime, timedelta

from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

from .models import Chat
from .tasks import send_notification_about_night_time, set_chat_permissions
from .utils import revoke_celery_task
from apps.commands.models import Command
from utils.redis import redis


@receiver(post_save, sender=Chat)
def set_default_commands_in_chat(instance, created, **kwargs):
    """Signal that sets default commands in chat. The default commands in chat are "experts" and "faq"."""
    if not created:
        return
    
    command_descriptions = {
        "en": {
            "experts": "Chat experts",
            "faq": "FAQ"
        },
        "ru": {
            "experts": "Эксперты чата",
            "faq": "Вопросы и ответы"
        }
    }

    try:
        user_key = f"language:{instance.main_admin.id}"
        language = redis.hget(name=user_key, key="language")
    except AttributeError:
        language = "ru"
    
    expert = Command(
        chat=instance,
        command_name="experts",
        command_description=command_descriptions[language]["experts"],
        number_calls=None,
        call_period="hour",
        delete_after_seconds=300,
        allowed_access="all",
    )
    faq = Command(
        chat=instance,
        command_name="faq",
        command_description=command_descriptions[language]["faq"],
        number_calls=None,
        call_period="hour",
        delete_after_seconds=300,
        allowed_access="all",
    )
    default_commands = [expert, faq]

    Command.objects.bulk_create(default_commands)


@receiver(pre_save, sender=Chat)
def chat_night_time_change(instance, **kwargs):
    """
    Signal that tracks changes in the start of night time or notofication about start night time in chat.
    If signal detects a changes it creates a new celery task `send_notification_about_night_time` and revoke 
    old celery task (if it was created).
    """
    try:
        previous_instance = Chat.objects.get(pk=instance.pk)

        if instance.night_time_on:
            if instance.start_night_time != previous_instance.start_night_time or instance.notification_time != previous_instance.notification_time or instance.notification_text != previous_instance.notification_text:
                chat_timezone = pytz.timezone(instance.timezone)
                current_time = datetime.now(chat_timezone)
                # Create notification sending time
                notification_sending_time = datetime.combine(current_time.date(), instance.start_night_time) - timedelta(seconds=instance.notification_time)
                notification_sending_time = chat_timezone.localize(notification_sending_time)
                if notification_sending_time < current_time:
                    notification_sending_time += timedelta(days=1)
                # Create a time at which the night time chat starts
                start_night_time = datetime.combine(current_time.date(), instance.start_night_time)
                start_night_time = chat_timezone.localize(start_night_time)
                if start_night_time < current_time:
                    start_night_time += timedelta(days=1)
                # Create a time at which the night time chat ends
                end_night_time = datetime.combine(current_time.date(), instance.end_night_time)
                end_night_time = chat_timezone.localize(end_night_time)
                if end_night_time < start_night_time:
                    end_night_time += timedelta(days=1)
                
                # Revoke celery tasks if they exist
                if instance.notification_celery_task_id:
                    revoke_celery_task(instance.notification_celery_task_id, immediate_revoke=True)
                if instance.start_night_time_celery_task_id:
                    revoke_celery_task(instance.start_night_time_celery_task_id, immediate_revoke=True)
                if instance.end_night_time_celery_task_id:
                    revoke_celery_task(instance.end_night_time_celery_task_id, immediate_revoke=True)
                
                # Run celery tasks
                notification_task = send_notification_about_night_time.apply_async((instance.tg_id, instance.notification_text), eta=notification_sending_time)
                start_night_time_task = set_chat_permissions.apply_async((instance.tg_id, "night"), eta=start_night_time)
                end_night_time_task = set_chat_permissions.apply_async((instance.tg_id, "current"), eta=end_night_time)
                instance.notification_celery_task_id = notification_task.id
                instance.start_night_time_celery_task_id = start_night_time_task.id
                instance.end_night_time_celery_task_id = end_night_time_task.id
        
        if not instance.night_time_on:
            if previous_instance.notification_celery_task_id:
                revoke_celery_task(previous_instance.notification_celery_task_id, immediate_revoke=True)
            if previous_instance.start_night_time_celery_task_id:
                revoke_celery_task(previous_instance.start_night_time_celery_task_id, immediate_revoke=True)
            if previous_instance.end_night_time_celery_task_id:
                revoke_celery_task(previous_instance.end_night_time_celery_task_id, immediate_revoke=True)
    except Chat.DoesNotExist:
        logging.error(f"Chat does not exist.")

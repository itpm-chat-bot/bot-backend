from django.contrib import admin

from .models import Chat


class ChatAdmin(admin.ModelAdmin):
    list_display = ("title", "tg_id", "is_bot_admin", "updated")
    list_filter = ("is_bot_admin", "chat_gpt_activated", "type")
    search_fields = ("title", "tg_id")
    ordering = ("-updated",)


admin.site.register(Chat, ChatAdmin)

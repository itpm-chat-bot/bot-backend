from django.db import models

from apps.chats.models import Chat


class ForbiddenWord(models.Model):
    """Stores forbidden word info"""

    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, db_index=True)
    word = models.CharField()
    ban_after_input = models.BooleanField(default=False)
    mute_after_input = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.word} (chat: {self.chat.title})"

from django.contrib import admin

from .models import ForbiddenWord


class ForbiddenWordsAdmin(admin.ModelAdmin):
    list_display = ("word", "chat", "ban_after_input", "mute_after_input", "created")
    list_filter = ("chat",)
    search_fields = ("word", "chat")
    ordering = ("-updated",)


admin.site.register(ForbiddenWord, ForbiddenWordsAdmin)

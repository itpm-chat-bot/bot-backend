from rest_framework import routers

from .views import ForbiddenWordViewSet

app_name = "forbidden_words"

router = routers.SimpleRouter()
router.register("", ForbiddenWordViewSet)

urlpatterns = router.urls

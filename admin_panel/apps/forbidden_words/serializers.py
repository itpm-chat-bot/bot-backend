from rest_framework.serializers import ModelSerializer

from .models import ForbiddenWord


class ForbiddenWordSerializer(ModelSerializer):
    class Meta:
        model = ForbiddenWord
        exclude = ("created", "updated")

import pytest

from django.contrib.auth import get_user_model
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import RefreshToken

from apps.chats.models import Chat
from apps.users.models import UserProfile
from core.settings import config

User = get_user_model()

USER_USERNAME = "test"
USER_USERNAME2 = "test2"
USER_PROFILE_ID = 1
USER_PROFILE2_ID = 2
CHAT_USER_ADMIN_TITLE = "Test"
CHAT_NO_ADMIN_TITLE = "Test_no_admins"
CHAT_USER2_ADMIN_TITLE = "Test_another_admin"
CHAT_USER_ADMIN_ID = 101
CHAT_USER2_ADMIN_ID = 102
CHAT_NO_ADMIN_ID = 103


@pytest.fixture
def users_data(db):
    """This fixture provides test data for users and user profiles in the database."""
    user = User.objects.create_user(username=USER_USERNAME)
    user2 = User.objects.create_user(username=USER_USERNAME2)
    UserProfile.objects.create(id=USER_PROFILE_ID, user=user, language_code="en")
    UserProfile.objects.create(id=USER_PROFILE2_ID, user=user2, language_code="en")


@pytest.fixture
def user_client(db, users_data):
    """Returns an authorized client"""
    user = User.objects.get(username=USER_USERNAME)
    refresh = RefreshToken.for_user(user)
    client = APIClient()
    client.credentials(HTTP_AUTHORIZATION=f"Bearer {refresh.access_token}")
    return client


@pytest.fixture
def bot_client(db):
    """Returns an authorized bot client"""
    client = APIClient()
    client.credentials(HTTP_AUTHORIZATION=config("BOT_TOKEN"))
    return client


@pytest.fixture
def chats_mock_data(db, users_data):
    """Adds mock data for chats"""
    chat1 = Chat.objects.create(
        id=CHAT_USER_ADMIN_ID, tg_id=CHAT_USER_ADMIN_ID, type="supergroup", title=CHAT_USER_ADMIN_TITLE
    )
    chat1.admins.add(USER_PROFILE_ID)
    chat2 = Chat.objects.create(
        id=CHAT_USER2_ADMIN_ID, tg_id=CHAT_USER2_ADMIN_ID, type="supergroup", title=CHAT_USER2_ADMIN_TITLE
    )
    chat2.admins.add(USER_PROFILE2_ID)
    Chat.objects.create(id=CHAT_NO_ADMIN_ID, tg_id=CHAT_NO_ADMIN_ID, type="supergroup", title=CHAT_NO_ADMIN_TITLE)
